var Serializable = require('./utils/utils.Serializable');
var config = require('./config/config');
//var Flow = require('./flow/flow.flow');
var mailer = require('./utils/utils.mailer');
var DateUtils = require('./utils/utils.date');
var NetSuiteOdbcMapper = require('./mapper/mapper.netsuiteodbcmapper');

process.on('uncaughtException', function(err){
	console.log('An unexpected error occurred');
	console.dir(err);
	process.exit(0);
});

runMe();

function runMe(){
	
	if(!process.argv[2]){
		return console.error('Flow name is required in command line argument');
	}
	
	var flowConfig = require('./config/flowconfigs/' + process.argv[2]);
	if(!flowConfig){
		return console.error('Invalid Flow name - ' + process.argv[2]);
	}
	
	console.log('reviving..');
	var flow = Serializable.revive(flowConfig);
	console.log('revived..');
	
	//console.log(flow instanceof Flow);
	
	flow.run()
	.then(function(){
		console.log('Flow completed successfully');
		process.exit(0);
	}, function(err){
		console.log('An error occured while flow excecution');
		console.error(err);
		process.exit(0);
	});
	
	/*mailer.sendMail(function(){
		console.log('sent mail');
	}, 'prasun.sultania@simplion.com', 'Test mail', 'This is a test mail');*/
}