
var Q = require('Q'),
	baseUrl,
	request = require('request'),
	config = require('../config/config');

/**
 * @param {Object}
 * @param {Object.username} - String required for credentials based authentication
 * @param {Object.password} - String required for credentials based authentication
 * @param {Object.role} - String required for credentials based authentication 
 * @param {Object.accountNumber} - String required for credentials based authentication
 * @param {Object.tokenPublic} - String required for token based authentication
 * @param {Object.tokenSecret} - String required for token based authentication
 * @param {Object.appPublic} - String required for token based authentication
 * @param {Object.appSecret} - String required for token based authentication
 * @param {Object.netsuiteRestletData} - String required for token based authentication
 */
exports.getNetSuiteRestletAuthHeader = function(args){

	var headerWithRealm;
	if(args.username && args.password && args.role){
		headerWithRealm = {};
		headerWithRealm.Authorization = "NLAuth nlauth_account=" + args.accountNumber + 
			", nlauth_email=" + args.username + 
			", nlauth_signature=" + args.password + 
			", nlauth_role=" + args.role;
	} else {
		var token = {
			public: args.tokenPublic,
			secret: args.tokenSecret
		};
		
		//app credentials
		var oauth = OAuth({
			consumer: {
				public: args.appPublic,
				secret: args.appSecret
			},
			signature_method: 'HMAC-SHA1'
		});
		
		var request_data = {
			url: args.netSuiteRestletData.getUrl(),
			method: args.netSuiteRestletData.getHttpMethod(),
			data: {}
		};
		
		headerWithRealm = oauth.toHeader(oauth.authorize(request_data, token));
		headerWithRealm.Authorization += ',realm=' + args.accountNumber;
	}
	
	//console.dir(headerWithRealm);
	
	headerWithRealm['Content-Type'] = 'application/json';
	return headerWithRealm;
};

/**
 * @param {Object}
 * @param {Object.authHeader} - String required
 */
exports.getNetSuiteRestletBaseUrl = function(args){
	var deferred = Q.defer();

	if(!args.authHeader){
		setTimeout(function(){
			return deferred.reject('Auth Header is required to fetch Restlet Base Url.');
		});
	} else if(!config.restRolesUrl){
		return deferred.reject('Please make sure restRolesUrl is defined in the configuration files.');
	} else if(baseUrl){
		setTimeout(function(){
			return deferred.resolve(baseUrl);
		});
	} else {
		request({
			url: config.restRolesUrl,
			method: 'GET',
			headers: args.authHeader
		}, function(error, response, body){
			if(error){
				console.dir(error);
				return deferred.reject(error);
			} else {
				try{
					var body2 = JSON.parse(response.body);
					if(body2.error){
						return deferred.reject(body2.error);
					}
					baseUrl = body2[0].dataCenterURLs.restDomain;
					console.log('baseUrl: ' + baseUrl);
					return deferred.resolve(baseUrl);
				} catch(e){
					console.dir(e.message);
					return deferred.reject("Invalid response for Base Url : " + body);
				}
			}
		});
	}
	
	return deferred.promise;
};