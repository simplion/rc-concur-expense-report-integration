/* jshint node: true */

'use strict';
var fs = require('fs');
var Q = require('q');
var _ = require('lodash');
var config = require('../config/config');

function createNewLogFile(args, deferred){
	fs.writeFile(args.fileName, args.data, function (err) {
		if (err) return deferred.reject(err);
		return deferred.resolve();
	});
}

function appendToLogFile(args, deferred){
	fs.appendFileSync(args.fileName, args.data);
}

exports.log = function(args){
	var deferred = Q.defer();

	(function(){
		var fileName,
			logData;

		if((!_.isNumber(args.count) && !_.isNumber(args.success) && !_.isNumber(args.errors)) || !_.isString(args.flowType)){
			return deferred.reject('Count and flowType are required');
		}

		if(args.flowType === 'mesflow'){
			fileName = config.mesLog;
		} else if(args.flowType === 'mescbflow') {
			fileName = config.mesCbLog;
		} else if(args.flowType === 'netsuiteflow'){
			fileName = config.netsuiteLog;
		} else if(args.flowType === 'concurflow'){
			fileName = config.concurLog;
		} else if(args.flowType === 'expenseflow'){
			fileName = config.expenseLog;
		} else {
			return deferred.reject('Invalid flow type : ' + args.flowType);
		}

		if(!fileName){
			return deferred.reject('Log file is not defined for flow Type - ' + args.flowType);
		}

		logData = (new Date()).toString() + '\t';

		if(_.isNumber(args.count)){
			logData += args.count;
		} else if(_.isNumber(args.success)){
			logData += 'Success: ' + args.success;
		}

		if(_.isNumber(args.errors)){
			logData += '\t' + ' error count:' + args.errors;
		}
		logData += '\n';

		// Start, added by Mohit Kumar for Expense Report on 18th May 2016
		if(args.DEBUG){
			logData += '\n\n\nDEBUG :\n' + args.DEBUG;
		}
		// END, added by Mohit Kumar for Expense Report on 18th May 2016

		try {
			fs.readFileSync(fileName);
			appendToLogFile({fileName: fileName,
					data: (logData)},
					deferred);
		} catch (ex) {

			console.log(ex);

			if(ex.code === 'ENOENT'){
				createNewLogFile({fileName: fileName,
					data: (logData)},
					deferred);
			} else {
				return deferred.reject('Failed to read log File - ' + fileName);
			}
		}
	})();

	return deferred.promise;
};
