'use strict';
var nodemailer = require('nodemailer');
var config = require('../config/config');
var Q = require('Q');

exports.sendMail = function(receiver, subject, emailHtml, attachments){
	var deferred = Q.defer();
  var smtpTransport = nodemailer.createTransport(config.mailer.options);
  console.log('sending mail ... :: ' + receiver);
  var mailOptions = {
    to: receiver,
    from: config.mailer.from,
    subject: subject,
    html: emailHtml + '<br/>Thank you, <br/>Simplion RC Team',
    attachments: attachments
  };
  console.log('sending mail ... :: ' + receiver);
  smtpTransport.sendMail(mailOptions, function(err) {
    console.log('mail sent');
    if(err){
    	console.log('Email err: ' + err);
    	return deferred.reject(err);
    }
    return deferred.resolve();
  });
  return deferred.promise;
};
