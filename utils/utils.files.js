'use strict';
var fs = require('fs');
var Q = require('q');

function merge(filePathArray, i,  mergedFileName, promise){
  var deferred = promise || Q.defer();
  var rs = fs.createReadStream(filePathArray[i]);
  var merged = fs.createWriteStream(mergedFileName, {flags: 'a'});
  
  if(i < filePathArray.length - 1){
    rs.on('end', function(){
      return merge(filePathArray, i+1,  mergedFileName, deferred);
    }).on('error', function(err){
      return deferred.reject(err);
    });
  } else {
    rs.on('end', function(){
      return deferred.resolve();
    }).on('error', function(err){
      return deferred.reject(err);
    });
  }
  
  rs.pipe(merged);
  return deferred.promise;
}

/**
 * Merges multiple files into one
 */
exports.concatFiles = function(filePathArray, mergedFileName){
  var deferred = Q.defer();
  if(!filePathArray || !mergedFileName || filePathArray.length < 2){
    throw new Error('Invalid arguments');
  }
  
  var merged = fs.createWriteStream(mergedFileName);
  var rs = fs.createReadStream(filePathArray[0]),
    i = 0;
  
  rs.on('end', function(){
    i++;
    merge(filePathArray, i,  mergedFileName).then(function(){
      return deferred.resolve();
    }, function(err){
      return deferred.reject(err);
    });
  });
  
  rs.pipe(merged);
  return deferred.promise;
};