var Q = require('Q');
var fs = require('fs');
var spawn = require('child_process').spawn;
var _ = require('lodash');
var MB = 1024*1024;
var config = require('../config/config');

function pdfk(args){
	var deferred = Q.defer(),
		consoleOutput = '',
		consoleError = '';
	
	setImmediate(function(){
		if(!_.isArray(args)){
			return deferred.reject('Invalid args');
		}
		
		var pdftk = spawn('pdftk', args);
		
		pdftk.stdout.on('data', function (data) {
			consoleOutput += data.toString().replace('\r\n', '\n');// .replace('\r\n', '\n');
		});
		
		pdftk.stderr.on('data', function (data) {
			consoleError += data;
		});
		
		pdftk.on('close', function (code) {
			if(code !==0){
				console.log('Failed PDF with code: ' + code);
				return deferred.reject(consoleError);
			}
			return deferred.resolve(consoleOutput);
		});
	});
	
	return deferred.promise;
}

function getFileSize(file){
	return fs.statSync(file).size/MB;
}

function getTotalNumberOfPages(file){
	var deferred = Q.defer();
	pdfk([file, 'dump_data']).then(
			function(consoleOuput){
				var pages = consoleOuput.substring(consoleOuput.lastIndexOf('PageMediaNumber:')).match(/\d+/);
				if(!pages){
					return deferred.reject('Failed to determine page number');
				}
				//console.log(consoleOuput);
				return deferred.resolve(parseInt(pages[0]));
			}, function(consoleError){
				console.dir(consoleError);
				return deferred.reject(consoleError);
			});
	return deferred.promise;
};

function splitFiles(fileName){
	var deferred = Q.defer();
	var splits = Math.ceil(getFileSize(config.dataLocation + fileName)/(config.pdfSplitFactor));
	console.log('splits: ' + splits);
		
	getTotalNumberOfPages(config.dataLocation + fileName).then(function(pages){
		var splitted = 0,
			errors = [];
			startPage = 1,
			lastPage = 0;
			
		console.log('pages: ' + pages);
			
		for(i = 0; i < splits; i++){
			lastPage = startPage + Math.floor(pages/splits);
			if(lastPage > pages){
				lastPage = pages;
			}
			console.log('startPage: ' + startPage);
			console.log('lastPage: ' + lastPage);
			pdfk(['A=' + (config.dataLocation + fileName), 
			      'cat', 
			      'A' + startPage + '-' + lastPage,
			      'output',
			      (config.dataLocation + fileName.replace('.pdf', '') + '_' + i + '.pdf')]).then(
					function(consoleOuput){
						splitted++;
						if((splitted + errors.length) >= splits && errors.length){
							return deferred.reject(errors);
						} else if(splitted >= splits){
							return deferred.resolve(splits);
						}
					}, function(consoleError){
						errors.push(consoleError);
						if((splitted + errors.length) >= splits){
							return deferred.reject(errors);
						}
					});
			startPage = lastPage + 1;
		}
	}, function(err){
		return deferred.reject(err);
	});
	return deferred.promise;
};

exports.getSplittedFiles = function(field){
	var deferred = Q.defer(),
		fileName;
	
	setImmediate(function(){
		try{
			if(!field || !field.getValue()){
				return deferred.resolve([]);
			} 
			fileName = field.getValue() + '.pdf';
			if(getFileSize(config.dataLocation + fileName) < 5){
				return deferred.resolve([fileName]);
			}
		} catch(ex){
			return deferred.reject('Failed to read file size for: ' + fileName + '. Due to -' + (ex.message || JSON.stringify(ex)));
		}
		
		splitFiles(fileName).then(function(splits){
			var fileNames = [],
				i = 0;
			try{
				for(; i < splits; i++){
					fileNames.push(fileName.replace('.pdf', '') + '_' + i + '.pdf');
				}
			} catch(ex){
				return deferred.reject('Failed to split file: ' + fileName + '. Due to -' + (ex.message || JSON.stringify(ex)));
			}
			console.dir('splitted files: ' + JSON.stringify(fileNames));
			return deferred.resolve(fileNames);
		}, function(ex){
			return deferred.reject('Failed to split file: ' + fileName + '. Due to -' + (ex.message || JSON.stringify(ex)));
		})
	});
	
	return deferred.promise;
};

exports.splitFiles = splitFiles;
exports.getFileSize = getFileSize;