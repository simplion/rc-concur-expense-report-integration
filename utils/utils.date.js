var DAY_MILLIS = 1000*60*60*24;

function getDateFromString(date){
	var splittedDate = date.split('-');
	return new Date(parseInt(splittedDate[0], 10), 
			parseInt(splittedDate[1], 10) - 1, 
			parseInt(splittedDate[2], 10));
}

function getDateInMmDdYyyy(date, join){
	var now = date || (new Date());
	var str = '';
	
	if(now.getMonth() + 1 < 10){
		str += '0' + (now.getMonth() + 1).toString();
	} else {
		str += (now.getMonth() + 1).toString();
	}
	
	if(join){
		str += join;
	}
	
	if(now.getDate() < 10){
		str += '0' + (now.getDate()).toString();
	} else {
		str += (now.getDate()).toString();
	}
	
	if(join){
		str += join;
	}
	
	str += now.getFullYear();
	return str;
};

function getDateInYyyyMmDd(date, join){
	var now = date || (new Date());
	var str = '';
	
	str += now.getFullYear();
	
	if(join){
		str += join;
	}
	
	if(now.getMonth() + 1 < 10){
		str += '0' + (now.getMonth() + 1).toString();
	} else {
		str += (now.getMonth() + 1).toString();
	}
	
	if(join){
		str += join;
	}
	
	if(now.getDate() < 10){
		str += '0' + (now.getDate()).toString();
	} else {
		str += (now.getDate()).toString();
	}
	
	return str;
};

exports.getDateInYyyyMmDd = getDateInYyyyMmDd;
exports.getDateInMmDdYyyy = getDateInMmDdYyyy;

exports.getTodayInMmDdYyyy = function(join){
	return getDateInMmDdYyyy(null, join);
};

exports.getTodayInYyyyMmDd = function(join){
	return getDateInYyyyMmDd(null, join);
};

/**
 * @return String date in SQL format eg: 2015-01-01
 */
exports.getSqlDateFromDate = function(date){
	var month = (date.getMonth() + 1);
	var d = date.getDate();
	
	month = month < 10 ? ('0' + month) : month;
	d = d < 10 ? ('0' + d) : d;
	
	return date.getFullYear() + '-' + month + '-' + d;
};

/*
 * @return Array of splitted date Strings
 */
exports.getSplittedDates = function(startDate, endDate, parts){
	if(!(/\d\d\d\d-\d\d-\d\d/.test(startDate)) || !(/\d\d\d\d-\d\d-\d\d/.test(endDate))){
		throw new Error('Invalid start date or end date');
	}
	
	if(!parts){
		throw new Error('Missing required argument - parts');
	}
	
	var d1 = getDateFromString(startDate),
		d2 = getDateFromString(endDate),
		tempEndDate = d1,
		prevEndDate = d1,
		dates = [];
	
	var daysDiff = (d2 - d1)/(DAY_MILLIS);	
	var dateDelta = Math.ceil(daysDiff/parts);
	//console.dir(d1);
	//console.dir(d2);
	do{
		prevEndDate = tempEndDate;
		//console.log(dateDelta*(1000*60*60*24));
		tempEndDate = new Date(tempEndDate.getTime() + (dateDelta*(DAY_MILLIS)));
		//console.dir(tempEndDate);
		if(tempEndDate > d2){
			tempEndDate = d2;
		}
		dates.push({
			d1: prevEndDate,
			d2: tempEndDate
		});
	} while(tempEndDate < d2);
	
	return dates;
}