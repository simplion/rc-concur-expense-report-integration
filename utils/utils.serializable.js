var _ = require('lodash');
var abc = require('../connector/connector.netsuiteodbcconnector');

/**
 * This class handles the Dependency Injection
 * @returns
 */
function Serializable(){};

/**
 * @return JSON representation of a class Object
 */
Serializable.prototype.toJSON = function(){
	var functions = _.functions(this),
		me = this,
		toReturn = {};

	if(!this.smplnClassName){
		throw new Error("Class must have a smplnClassName Attribute");
	}

	//console.log('toJSON on ' + this.smplnClassName + ' functions: ' + functions.toString());
	functions.forEach(function(fun){
		var value;

		if(fun.indexOf('get') === 0 && fun.length > 3){
			value = JSON.stringify(me[fun]());
			if(value !== undefined){
				toReturn[fun.substring(3, 4).toLowerCase() + fun.substring(4)] = JSON.parse(value);
			}
		} else if(fun.indexOf('is') === 0 && fun.length > 2){
			value = JSON.stringify(me[fun]());
			if(value !== undefined){
				toReturn[fun.substring(2, 3).toLowerCase() + fun.substring(3)] = JSON.parse(value);
			}
		}
	});
	return toReturn;
};

Serializable.prototype.getSmplnClassName = function(){
	return this.smplnClassName;
};

Serializable.prototype.setSmplnClassName = function(smplnClassName){
	this.smplnClassName = smplnClassName;
};

Serializable.prototype.stringify = function(){
	JSON.stringify(this.toJSON());
}

/**
 * It instantiates an Object from its JSON representation
 * And also instantiates all the dependent Objects recursively.
 * @return Instance of class from a JSON
 */
Serializable.revive = function(jsonString){

    var jsonObj = _.isString(jsonString) ? JSON.parse(jsonString) : jsonString,
        keys = Object.keys(jsonObj), //array of keys
        classObj,
        classLocation,
        setterMethod,
        value,
        tempArr=[];

    if (!jsonObj.hasOwnProperty('smplnClassName')) {
        return jsonObj;
    }

    classLocation = '../' + jsonObj['smplnClassName'].replace(/\./g,'/');
    //console.log('classLocation 1:' + classLocation);
    classLocation = classLocation.substring(0, classLocation.lastIndexOf('/')) + "/" + jsonObj['smplnClassName'];
    //console.log('classLocation 2:' + classLocation);

    classObj = require(classLocation); //load the class object basis the className value

    if(!_.isFunction(classObj)){
    	throw new Error('Class not found: ' + jsonObj.smplnClassName);
    }

    classObj = new classObj({});
    //nlapiLogExecution('DEBUG', 'Reviving : '  + jsonObj['smplnClassName'], JSON.stringify(jsonObj));

    _.each(keys, function (key) {

        if(key === 'smplnClassName'){
            return;
        }

        value = jsonObj[key];
        setterMethod = 'set' + key.charAt(0).toUpperCase() + key.slice(1);

        //console.log('setter method: ' + setterMethod);

        if(!_.isFunction(classObj[setterMethod])){
        	console.log('Reviver warning: No function for key',
        			key + " in " + jsonObj.smplnClassName + ' . You might want to remove this key from json, in case key is not an attribute on class');
        	return;
        }

        try {
        //primitive data types
	        if (!_.isObject(value)) {
	            classObj[setterMethod](value);
	            return;
	        }

	        //Non-array objects
	        if(_.isObject(value) && !_.isArray(value)){
	            classObj[setterMethod](Serializable.revive(value));
	            return;
	        }

	        //array objects
	        if(_.isArray(value)){
	        	try{
	            _.each(value, function(arrObj){
	                if(_.isObject(arrObj)){
	                    tempArr.push(Serializable.revive(arrObj));
	                } else {
	                    tempArr.push(arrObj);
	                }
	            });
	            classObj[setterMethod](tempArr);
	            tempArr = [];
	        	} catch(e){
	        		console.error('Failed in reviving Arr setter method in ' + jsonObj.smplnClassName, setterMethod);
	        		console.error('Failed in reviving Arr setter method in ' + jsonObj.smplnClassName, key);
	        		console.log('Failed in reviving Arr setter method in '  + jsonObj['smplnClassName'], JSON.stringify(value));
	        		tempArr.forEach(function(tArr, k) {
	        			console.error('Failed in reviving Arr [tArr] ' + k, tArr);
					});
	        		throw e;
	        	}
	        }
        } catch(ex){
        	console.error('Failed in reviving setter method in ' + jsonObj.smplnClassName, setterMethod);
        	console.error('Failed in reviving setter method in '  + jsonObj['smplnClassName'], JSON.stringify(jsonObj));
        	throw ex;
        }

    });

    return classObj;
};

module.exports = Serializable;
