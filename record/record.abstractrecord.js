var _ = require('lodash');
var util = require('util');
var Serializable = require('../utils/utils.serializable');
//var ChildRecord = require('./record.childrecord');

/**
 * Class represents a Record data
 * @param args
 * @returns {AbstractRecord}
 */
function AbstractRecord(args) {

    AbstractRecord.super_.call(this, args);

    this._label = args.label;
    this._recordId = args.recordId;
    this._recordType = args.recordType;

    // Start Added By Mohit Kumar for expense report record on 11th July 2016
    this._flowName = args.flowName;
    // END Added By Mohit Kumar for expense report record on 11th July 2016

    this._fieldValueMap = {};
    /** Array of field.Field **/
    this._fields = args.fields || [];
    /** Array of record.ChildRecord **/
    this._childRecords = args.childRecords || [];

    var that = this;
    _.each(this._fields, function(field){
    	if(!field){
    		throw new Error('You cannot set a field as null on ' + that._recordType);
    	}
        that._fieldValueMap[field.getApiName()] = field;
    });

    this._creatable = args.creatable;
    this._readable = args.readable;
    this._updatable = args.updatable;
    this._deletable = args.deletable;

    this._platform = args.platform;
    this.smplnClassName = 'record.abstractrecord';

};
util.inherits(AbstractRecord, Serializable);

AbstractRecord.prototype._addToFieldMap = function(name, field) {
    this._fieldValueMap[name] = field;
};

AbstractRecord.prototype._removeFromFieldMap = function(name, value) {
    delete this._fieldValueMap[name];
};

/**
 * @returns Object required by the platform
 * eg: nlobjRecord in case of NetSuite and JSON record in case of SFDC, as required by sfdcCrudManager class
 */
AbstractRecord.prototype.toPlatformRecordObject = function(){
    throw new Error('This method must be overriden');
};

AbstractRecord.prototype.getRecordType = function(){
    return this._recordType;
};

AbstractRecord.prototype.setRecordType = function(recordType){
    this._recordType = recordType;
};

// Start Added By Mohit Kumar for expense report record on 11th July 2016
AbstractRecord.prototype.getFlowName = function(){
    return this._flowName;
};

AbstractRecord.prototype.setFlowName = function(flowName){
    this._flowName = flowName;
};
// END Added By Mohit Kumar for expense report record on 11th July 2016

AbstractRecord.prototype.getLabel = function(){
    return this._label;
};

AbstractRecord.prototype.setLabel = function(label){
    this._label = label;
};

AbstractRecord.prototype.getRecordId = function(){
    return this._recordId;
};

AbstractRecord.prototype.setRecordId = function(recordId){
    this._recordId = recordId;
};

AbstractRecord.prototype.getPlatform = function(){
    return this._platform;
};

AbstractRecord.prototype.setPlatform = function(platform){
    this._platform = platform;
};

AbstractRecord.prototype.getFields = function(){
    return this._fields;
};

AbstractRecord.prototype.getChildRecords = function(){
    return this._childRecords;
};

AbstractRecord.prototype.setChildRecords = function(childRecords){
    this._childRecords = childRecords;
};

AbstractRecord.prototype.addChildRecord = function(childRecord){
    this._childRecords.push(childRecord);
};

AbstractRecord.prototype.removeChildRecord = function(relationshipName){
    this._childRecords = _.filter(_childRecords, function(childRecord) {
        return childRecord.getRelationshipName() !== relationshipName;
    });
};

AbstractRecord.prototype.setFields = function(fields){
    var that = this;
    this._fields = fields;
    var ChildRecord = require('./record.childrecord');
    _.each(fields, function(field){
    	if(field instanceof ChildRecord){
    		throw new Error('You cannot set a field as null on ' + that.getRecordType());
    	}
    	try{
    		that._addToFieldMap(field.getApiName(), field);
    	} catch(ex){
    		//nlapiLogExecution('ERROR', 'get ApiName failed ' + that.getRecordType(), ex);
    		//nlapiLogExecution('ERROR', 'get ApiName failed ' + that.getRecordType(), JSON.stringify(field));
    		//nlapiLogExecution('ERROR', 'get ApiName failed ' + that.getRecordType(), JSON.stringify(fields));
    		throw ex;
    	}
    });
};

AbstractRecord.prototype.addField = function(field){
    this._fields.push(field);
    this._addToFieldMap(field.getApiName(), field);
};

AbstractRecord.prototype.readField = function(fieldApiName){
    var field = this._fieldValueMap[fieldApiName];
    if(!field){
    	throw new Error('No such field - ' + fieldApiName +
    			' exists in this record - ' + this.getRecordType());
    }
    return field;
};

AbstractRecord.prototype.removeField = function(index){
    index = index || this._fields.length - 1;

    var field = this._fields.splice(index, 1);
    this._removeFromFieldMap(field.getApiName());
};

AbstractRecord.prototype.isCreatable = function(){
    return this._creatable;
};

AbstractRecord.prototype.setCreatable = function(creatable){
    this._creatable = creatable;
};

AbstractRecord.prototype.isReadable = function(){
    return this._readable;
};

AbstractRecord.prototype.setReadable = function(readable){
    this._readable = readable;
};

AbstractRecord.prototype.isUpdatable = function(){
    return this._updatable;
};

AbstractRecord.prototype.setUpdatable = function(updatable){
    this._updatable = updatable;
};

AbstractRecord.prototype.isDeletable = function(){
    return this._deletable;
};

AbstractRecord.prototype.setDeletable = function(deletable){
    this._deletable = deletable;
};

module.exports = AbstractRecord;
