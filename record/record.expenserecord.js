var util = require('util');
var _ = require('lodash');
var AbstractRecord = require('./record.abstractrecord');


function ExpenseRecord(args) {

    ExpenseRecord.super_.call(this, args);

    this.smplnClassName = 'record.concurrecord';

}

util.inherits(ExpenseRecord, AbstractRecord);

ExpenseRecord.prototype.toPlatformRecord = function () {
    throw new Error('Not yet implemented');
};


module.exports = ExpenseRecord;
