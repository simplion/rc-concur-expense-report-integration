var ConcurRecordBuilder = {};

var csv = require('csv-parser');
var fs = require('fs');
var Q = require('Q');
var Field = require('../field/field.Field');
var _ = require('lodash');
var config = require('../config/config');

var ConcurRecord = require('./record.concurrecord');
var ChildRecord = require('./record.childrecord');

var HEADERS = ["Concur field", "Request Key", "Policy Name", "Title", "Description",
	        "Vendor Invoice Number", "Invoice Date", "Payment Due Date", "Invoice Amount",
	        "Total Amount", "Creation Date", "Submit Date", "PO Number", "Request Custom 1",
	        "Request Custom 2", "Request Custom 3", "Request Custom 4", "Request Custom 5",
	        "Request Custom 6", "Request Custom 7", "Request Custom 8", "Request Custom 9",
	        "Request Custom 10", "Request Custom 11", "Request Custom 12", "Request Custom 13",
	        "Request Custom 14", "Request Custom 15", "Request Custom 16", "Request Custom 17",
	        "Request Custom 18", "Request Custom 19", "Request Custom 20", "Request Custom 21",
	        "Request Custom 22", "Request Custom 23", "Request Custom 24", "Request Og Unit 1",
	        "Request Og Unit 2", "Request Og Unit 3", "Request Og Unit 4", "Request Og Unit 5",
	        "Request Og Unit 6", "Notes to Approvers", "Notes to Vendor", "Request Group Code",
	        "Request Group Name", "Image Available", "Request Shipping Amount", "Request Tax Amount",
	        "Request ID", "Invoice Received Date", "Payment Method Type", "Future-use 6",
	        "Future-use 7", "Future-use 8", "Future-use 9", "Future-use 10", "Ledger Code",
	        "Ledger name", "Journal Account Code", "Journal Amount", "Journal Gross Amount",
	        "Debit or Credit", "Requested Alpha Currency Code", "Requested Numeric Currency Code",
	        "Request Currency Name", "Allocation Account Code", "Allocation Custom 1",
	        "Allocation Custom 2", "Allocation Custom 3", "Allocation Custom 4", "Allocation Custom 5",
	        "Allocation Custom 6", "Allocation Custom 7", "Allocation Custom 8", "Allocation Custom 9",
	        "Allocation Custom 10", "Allocation Custom 11", "Allocation Custom 12", "Allocation Custom 13",
	        "Allocation Custom 14", "Allocation Custom 15", "Allocation Custom 16", "Allocation Custom 17",
	        "Allocation Custom 18", "Allocation Custom 19", "Allocation Custom 20", "Allocation Key",
	        "Future-use 12", "Future-use 13", "Future-use 14", "Future-use 15", "Employee ID",
	        "Employee Login ID", "Employee First Name", "Employee Middle Initial", "Employee Last Name",
	        "Employee Email Address", "Employee Custom 1", "Employee Custom 2", "Employee Custom 3",
	        "Employee Custom 4", "Employee Custom 5", "Employee Custom 6", "Employee Custom 7",
	        "Employee Custom 8", "Employee Custom 9", "Employee Custom 10", "Employee Custom 11",
	        "Employee Custom 12", "Employee Custom 13", "Employee Custom 14", "Employee Custom 15",
	        "Employee Custom 16", "Employee Custom 17", "Employee Custom 18", "Employee Custom 19",
	        "Employee Custom 20", "Employee Org Unit 1", "Employee Org Unit 2", "Employee Org Unit 3",
	        "Employee Org Unit 4", "Employee Org Unit 5", "Employee Org Unit 6", "Future-use 16",
	        "Future-use 17", "Future-use 18", "Associated PO Line Item Number", "Future-use 20", "Number Line Items",
	        "Line Item Sequence Order", "Line Item Description", "Line Item Expense Type Code",
	        "Line Item Expense Type Name", "Line Item Quantity", "Line Item Unit Price",
	        "Line Item Total", "Line Item Custom 1", "Line Item Custom 2", "Line Item Custom 3",
	        "Line Item Custom 4", "Line Item Custom 5", "Line Item Custom 6", "Line Item Custom 7",
	        "Line Item Custom 8", "Line Item Custom 9", "Line Item Custom 10", "Line Item Custom 11",
	        "Line Item Custom 12", "Line Item Custom 13", "Line Item Custom 14", "Line Item Custom 15",
	        "Line Item Custom 16", "Line Item Custom 17", "Line Item Custom 18", "Line Item Custom 19",
	        "Line Item Custom 20", "Line Item Ship From Zip Code", "Line Item Ship To Zip Code",
	        "Line Item Tax", "Supplier Part ID", "Future-use 25", "Vendor Name", "Vendor Code",
	        "Vendor Remit To Address Code", "Vendor Remit To Address ID", "Vendor Remit To Address 1",
	        "Vendor Remit To Address 2", "Vendor Remit To Address 3", "Vendor Remit To City",
	        "Vendor Remit To State", "Vendor Remit To Postal Code", "Vendor Remit To Country",
	        "Vendor Remit To Contact First Name", "Vendor Remit To Contact Last Name",
	        "Vendor Remit To Phone Number", "Vendor Remit To Custom 1", "Vendor Remit To Custom 2",
	        "Vendor Remit To Custom 3", "Vendor Remit To Custom 4", "Vendor Remit To Custom 5",
	        "Vendor Remit To Custom 6", "Vendor Remit To Custom 7", "Vendor Remit To Custom 8",
	        "Vendor Remit To Custom 9", "Vendor Remit To Custom 10", "Vendor Remit To Custom 11",
	        "Vendor Remit To Custom 12", "Vendor Remit To Custom 13", "Vendor Remit To Custom 14",
	        "Vendor Remit To Custom 15", "Vendor Remit To Vendor Location Image Received",
	        "Company Billing Address Tax Id", "Future-use 27", "Future-use 28", "Future-use 29",
	        "Future-use 30", "Vendor Ship From Address Code", "Vendor Ship From Address ID",
	        "Vendor Ship From Address 1", "Vendor Ship From Address 2", "Vendor Ship From Address 3",
	        "Vendor Ship From City", "Vendor Ship From State", "Vendor Ship From Zip Code",
	        "Vendor Ship From Country Code", "Vendor Ship From Contact First Name",
	        "Vendor Ship From Contact Last Name", "Vendor Ship From Phone Number",
	        "Vendor Ship From Custom 1", "Vendor Ship From Custom 2", "Vendor Ship From Custom 3",
	        "Vendor Ship From Custom 4", "Vendor Ship From Custom 5", "Vendor Ship From Custom 6",
	        "Vendor Ship From Custom 7", "Vendor Ship From Custom 8", "Vendor Ship From Custom 9",
	        "Vendor Ship From Custom 10","Vendor Ship From Custom 11", "Vendor Ship From Custom 12",
	        "Vendor Ship From Custom 13", "Vendor Ship From Custom 14", "Vendor Ship From Custom 15",
	        "Vendor Ship From Vendor Location Image Received", "Payment Demand Company Liability Account Code",
	        "Payment Demand Company Cash Account Code", "Estimated Payment Date",
	        "Is Request Emergency Check Run", "Future-use 35", "Future-use 36", "Future-use 37",
	        "Future-use 38", "Future-use 39", "Future-use 40", "Future-use 41", "Future-use 42",
	        "Future-use 43", "Future-use 44", "Future-use 45", "Future-use 46", "Future-use 47",
	        "Future-use 48", "Future-use 49", "Future-use 50", "Future-use 51", "Future-use 52",
	        "Future-use 53", "Future-use 54", "Future-use 55", "Future-use 56", "Future-use 57",
	        "Future-use 58","Future-use 59", "Future-use 60"];

var LINE_INFO = ["Journal Account Code",
                 "Allocation Custom 2",
                 "Line Item Description",
                 "Line Item Quantity",
                 "Line Item Unit Price",
                 "Line Item Total",
                 "Line Item Custom 1",
                 "Line Item Custom 2",
                 "Line Item Custom 3",
                 "Line Item Custom 4",
                 "Journal Amount",
                 "Associated PO Line Item Number"];

function getFieldObjects(fields){
	var fieldsObjects = {
			lineFields: [],
			headerFields : []
	};

	fields.forEach(function(field, i){
		if(LINE_INFO.indexOf(HEADERS[i]) !== -1){
			//console.log('line field :: '  + HEADERS[i]);
			fieldsObjects.lineFields.push(new Field({
				value: field,
				apiName: HEADERS[i],
				type: 'String'
			}));
		} else {
			if(config.invoicePrefix && HEADERS[i] === 'Vendor Invoice Number'){
				field = config.invoicePrefix + field;
				//console.log(field);
			}
			fieldsObjects.headerFields.push(new Field({
				value: field,
				apiName: HEADERS[i],
				type: 'String'
			}));
		}
	});

	return fieldsObjects;
}

function getNewRecord(fieldsObjects){
	return new ConcurRecord({
		recordType: 'invoice',
		fields: fieldsObjects.headerFields,
		childRecords : [new ChildRecord({
			fields: fieldsObjects.lineFields,
			childType: 'sublist',
			childIndex: 1,
			relationshipName: 'lineitem'
		})]
	});
}

function mergeJsonRecords(oldRecord, fieldsObjects){
	//console.log('adding child');
	oldRecord.addChildRecord(new ChildRecord({
		fields: fieldsObjects.lineFields,
		childType: 'sublist',
		childIndex: (oldRecord.getChildRecords().length + 1),
		relationshipName: 'lineitem'
	}));
}

function getJsonRecords(concurFile){
	var deferred = Q.defer();
	var jsonRecordKeyToRecord = {},
		rows,
		records,
		lineFields,
		headerFields,
		fields;

	try{

		// Start, added by Mohit Kumar for PO same linenumber change on 13th April 2016
		// concurFile = "D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\data\\temp_expense.txt";
		console.log('concurFile :' + concurFile);
		// process.exit(0);
		// END, added by Mohit Kumar for PO same linenumber change on 13th April 2016

		fs.readFile(concurFile, function(err, data){
			if(err){
				return deferred.reject(err.message);
			}

			data = data.toString();

			if(data.indexOf('EXTRACT') !== 0){
				return deferred.reject('Invalid file content received from Concur - ' + data);
			}

			//console.log('removing line 1 ...');

			//remove 1st line
			data = data.replace(/[^\r\n]+\r\n/,'');
			rows = data.split('\r\n');
			//flush data
			data = '';

			console.log('processing rows ...');
			rows.forEach(function(row, i){
				var fieldsObject;
				if(!row){return;}
				fields = row.split('|');
				fieldsObject = getFieldObjects(fields);
				if(jsonRecordKeyToRecord[fieldsObject.headerFields[1].getValue()]){
					mergeJsonRecords(jsonRecordKeyToRecord[fieldsObject.headerFields[1].getValue()], fieldsObject);
				} else {
					jsonRecordKeyToRecord[fieldsObject.headerFields[1].getValue()] = getNewRecord(fieldsObject);
				}
				//console.log('processed row: ' + i)
			});

			// console.log('Total records : ' + records.length);

			//fix PO# information - for NO-PO Invoices
			records = _.map(_.values(jsonRecordKeyToRecord), function(record){
				if(record.readField('Policy Name').getValue() && (
						record.readField('Policy Name').getValue().toLowerCase() === 'ring central non-po invoice policy' ||
						record.readField('Policy Name').getValue().toLowerCase() === 'ring central missing po invoice policy')){
					if(record.readField('PO Number').getValue()){
						console.log('PO fixed - ' + record.readField('Request Key').getValue());
						record.readField('PO Number').setValue(undefined);
					}
				} /*else {
					console.log('No PO fixed - ' + record.readField('Request Key').getValue() + '\t' + record.readField('Policy Name').getValue());
				}*/

				if(record.readField('Request Key').getValue() === undefined){
					console.log('Warning! A record without request key was found');
				}

				return record;
			});

			/*if(config.maxRecords){
				records = records.splice(config.maxRecords);
			}*/

			console.log('Total records : ' + records.length);

			if(_.isArray(config.testRequestKeys)){
				records = _.filter(records, function(record){
					return config.testRequestKeys.indexOf(record.readField('Request Key').getValue()) !== -1;
				});
			}

			console.log('Total records after test filter : ' + records.length);

			if(config.filterNonPoRecords){
				records = _.filter(records, function(jsonRecord){
					if(jsonRecord.readField('PO Number').getValue()){
						console.log('Choosing Po - ' + jsonRecord.readField('Request Key').getValue() + ' :: ' +
								jsonRecord.readField('Vendor Invoice Number').getValue() + ' :: ' +
								jsonRecord.readField('PO Number').getValue());
						return true;
					} else {
						/*console.log('filtered Non Po - ' + jsonRecord.readField('Request Key').getValue() + ' :: ' +
								jsonRecord.readField('Vendor Invoice Number').getValue());*/
						return false;
					}
				});
			}

			if(config.filterNonPoLineNumber){
				records = _.filter(records, function(jsonRecord){
					if(jsonRecord.readField('PO Number').getValue()){
						var allowed = false;
						var childLineNumbers = [];

						jsonRecord.getChildRecords().forEach(function(childRec){
							if(childRec.readField('Associated PO Line Item Number').getValue()){
								allowed = true;
								childLineNumbers.push(childRec.readField('Associated PO Line Item Number').getValue());
							}
						});

						if(allowed){
							console.log('Choosing Line numbered Po - ' + jsonRecord.readField('Request Key').getValue() + ' :: ' +
									jsonRecord.readField('Vendor Invoice Number').getValue() + ' :: ' +
									jsonRecord.readField('PO Number').getValue() + ' :: ' +
									JSON.stringify(childLineNumbers));
						}
						return allowed;
					} else {
						/*console.log('filtered Non Po - ' + jsonRecord.readField('Request Key').getValue() + ' :: ' +
								jsonRecord.readField('Vendor Invoice Number').getValue());*/
						return false;
					}
				});
			}

			//Fix more than 1 Bill line items matching to same one on PO
			_.each(records, function(jsonRecord){
				if(jsonRecord.readField('PO Number').getValue()){
					var lineNumbers = [];
					jsonRecord.getChildRecords().forEach(function(childRec){
						if(lineNumbers.indexOf(childRec.readField('Associated PO Line Item Number').getValue()) !== -1){
							console.log('Fixed Line number for PO#' + jsonRecord.readField('PO Number').getValue() +
									' Line#' + childRec.readField('Associated PO Line Item Number').getValue());

							// Start, commented by Mohit Kumar for PO same linenumber change on 13th April 2016
							// childRec.readField('Associated PO Line Item Number').setValue(null);
							// END, commented by Mohit Kumar for PO same linenumber change on 13th April 2016
						} else if(childRec.readField('Associated PO Line Item Number').getValue()){
							lineNumbers.push(childRec.readField('Associated PO Line Item Number').getValue());
						}
					});

				}
				// Start, commented by Mohit Kumar for Non-PO record change on 31st May 2016
				else {
					console.log('filtered Non Po - ' + jsonRecord.readField('Request Key').getValue() + ' :: ' +
							jsonRecord.readField('Vendor Invoice Number').getValue());
					// return false;
				}
				// END, commented by Mohit Kumar for Non-PO record change on 31st May 2016
			});


			if(config.filterPoRecords){
				//console.log('filtering Po records 2');
				return deferred.resolve(_.filter(records, function(jsonRecord){
					try{
						if(jsonRecord.readField('PO Number').getValue()){
							console.log('filtered - ' + jsonRecord.readField('Request Key').getValue() + ' :: ' +
									jsonRecord.readField('Vendor Invoice Number').getValue());
							return false;
						} else {
							return true;
						}
					} catch(ex){
						console.log(ex.messgae);
						throw ex;
					}
				}));
			} else {
				//console.dir(JSON.stringify(records));
				return deferred.resolve(records);
			}
		});
	} catch(ex) {
		return deferred.reject(ex.message);
	}

	return deferred.promise;
}

ConcurRecordBuilder.getConcurRecords = function(file){
	return getJsonRecords(file);
};

module.exports = ConcurRecordBuilder;
