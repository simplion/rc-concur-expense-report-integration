var ExpenseRecordBuilder = {};

var csv = require('csv-parser');
var fs = require('fs');
var Q = require('Q');
var Field = require('../field/field.Field');
var _ = require('lodash');
var config = require('../config/config');

var ExpenseRecord = require('./record.expenserecord');
var ChildRecord = require('./record.childrecord');

var SyncLogger = require('../utils/utils.synclogger');

var HEADERS = ["Constant", "Batch ID", "Batch Date", "Sequence Number",
    "Employee ID", "Employee Last Name", "Employee First Name", "MI", "Employee Group ID (Custom 21)",
    "Employee Org Unit 1", "Employee Org Unit 2", "Employee Org Unit 3", "Employee Org Unit 4",
    "Employee Org Unit 5", "Employee Org Unit 6", "ACH Bank Account Number", "ACH Bank Routing Number",
    "Journal Net of Total Adjusted Reclaim Tax", "Concur Expense Report ID", "Report Key", "Ledger",
    "Reimbursement Currency Alpha ISE", "Home Country", "Date", "Report User Defined Date",
    "Report Payment Processing Date", "Report Name", "Report Image Required", "Report has VAT entry",
    "Report has TA entry", "Report Total Post Amount", "Report Total Approved Amount", "Report Policy Name",
    "Report Entry Budget Accrual Date", "Report Org Unit 1", "Report Org Unit 2", "Report Org Unit 3",
    "Report Org Unit 4", "Report Org Unit 5", "Report Org Unit 6", "Report Custom 1", "Report Custom 2",
    "Report Custom 3", "Report Custom 4", "Report Custom 5", "Report Custom 6", "Report Custom 7",
    "Report Custom 8", "Report Custom 9", "Report Custom 10", "Report Custom 11", "Report Custom 12",
    "Report Custom 13", "Report Custom 14", "Report Custom 15", "Report Custom 16", "Report Custom 17",
    "Report Custom 18", "Report Custom 19", "Report Custom 20", "Report Entry Id",
    "Report Entry Transaction Type", "Line Item Category", "Line Item Expense Date",
    "Line Item Currency", "Line Item Exchange Rate", "Report Entry Exchange Rate Direction",
    "Report Entry Is Personal Flag", "Purpose", "Report Entry Vendor Name",
    "Line Item Description", "Report Entry Receipt Received Flag", "Report Entry Receipt Type",
    "Total Employee Attendee", "Total Spouse Attendee", "Total Business Attendee", "Report Entry Org Unit 1",
    "Report Entry Org Unit 2", "Report Entry Org Unit 3", "Report Entry Org Unit 4", "Report Entry Org Unit 5",
    "Report Entry Org Unit 6", "Report Entry Custom 1", "Report Entry Custom 2", "Report Entry Custom 3",
    "Report Entry Custom 4", "Report Entry Custom 5", "Report Entry Custom 6", "Report Entry Custom 7",
    "Report Entry Custom 8", "Report Entry Custom 9", "Report Entry Custom 10", "Report Entry Custom 11",
    "Report Entry Custom 12", "Report Entry Custom 13", "Report Entry Custom 14", "Report Entry Custom 15",
    "Report Entry Custom 16", "Report Entry Custom 17", "Report Entry Custom 18", "Report Entry Custom 19",
    "Report Entry Custom 20", "Report Entry Custom 21", "Report Entry Custom 22", "Report Entry Custom 23",
    "Report Entry Custom 24", "Report Entry Custom 25", "Report Entry Custom 26", "Report Entry Custom 27",
    "Report Entry Custom 28", "Report Entry Custom 29", "Report Entry Custom 30", "Report Entry Custom 31",
    "Report Entry Custom 32", "Report Entry Custom 33", "Report Entry Custom 34", "Report Entry Custom 35",
    "Report Entry Custom 36", "Report Entry Custom 37", "Report Entry Custom 38", "Report Entry Custom 39",
    "Report Entry Custom 40", "Line Item Amount", "Report Entry Posted Amount",
    "Report Entry Approved Amount", "Payment Type Code", "Payment Code", "Line Item Non Reimbursable",
    "Bill Date", "Billed Credit Card Account Number", "Billed Credit Card Description",
    "Credit Card Transaction JR Key", "Credit Card Transaction Reference Number",
    "Credit Card Transaction CCT Key", "Credit Card Transaction CCT Type", "Credit Card Transaction ID",
    "Credit Card Transaction Amount", "Credit Card Transaction Tax Amount",
    "Credit Card Transaction Currency Alpha Code", "Credit Card Transaction Posted Amount",
    "Credit Card Transaction Posted Currency Alpha Code", "Credit Card Transaction Date",
    "Credit Card Transaction Posted Date", "Credit Card Transaction Description",
    "Credit Card Transaction Master Card Code", "Credit Card Transaction Merchant Name",
    "Credit Card Transaction Merchant City", "Credit Card Transaction Merchant State",
    "Credit Card Transaction Merchant Country Code", "Credit Card Transaction Merchant Reference Number",
    "Credit Card Transaction Billing Type", "Exchange Rate From Billing To Employee Currency",
    "Billing Amount", "Individual Credit Card Account Number", "Individual Credit Card Name On Card",
    "Merchant Doing Business As", "Acquirer Reference Number", "Report Entry Location Country Code",
    "Report Entry Location Country Sub Code", "Report Entry Foreign or Domestic Flag", "Market Code",
    "Processor Reference Number", "Journal Payer Payment Type Name", "Journal Payer Payment Code Name",
    "Journal Payee Payment Type Name", "Journal Payee Payment Code Name", "Journal Account Code",
    "Journal Debit or Credit", "Journal Amount", "Journal Key", "Car Business Distance",
    "Car Personal Distance", "Car Passenger Count", "Vehicle ID", "Credit Card Transaction Sales",
    "Credit Card Vendor Name", "Cash Advance Request Amount", "Cash Advance Request Currency Alpha Code",
    "Cash Advance Request Currency Numeric Code", "Cash Advance Exchange Rate",
    "Cash Advance Currency Alpha Code", "Cash Advance Currency Numeric Code", "Cash Advance Issued Date",
    "Cash Advance Payment Code Name", "Cash Advance Transaction Type", "Cash Advance Request Date",
    "Cash Advance Key", "Cash Advance Payment Method", "Allocation Key", "Allocation Percentage",
    "Allocation Custom 1", "Line Item Department", "Allocation Custom 3", "Allocation Custom 4",
    "Allocation Custom 5", "Allocation Custom 6", "Allocation Custom 7", "Allocation Custom 8",
    "Allocation Custom 9", "Allocation Custom 10", "Allocation Custom 11", "Allocation Custom 12",
    "Allocation Custom 13", "Allocation Custom 14", "Allocation Custom 15", "Allocation Custom 16",
    "Allocation Custom 17", "Allocation Custom 18", "Allocation Custom 19", "Allocation Custom 20",
    "Journal Net of Total Adjusted Tax", "TA Reimb. Meal Lodging or Combined Type", "TA Display Limit",
    "TA Allowance Limit", "Allowable Threshold", "TA Fixed Meal Lodging Type", "Base Amount",
    "Allowance Amount", "TA Fixed Overnight", "TA Fixed Breakfast Provided Flag",
    "TA Fixed Lunch Provided Flag", "TA Fixed Dinner Provided Flag", "Total Tax Adjusted Posted Amount",
    "Total Reclaim Adjusted Amount", "Tax Authority Name", "Tax Authority Label",
    "Report Entry Tax Transaction Amount", "Report Entry Tax Posted Amount", "Tax Source",
    "Report Entry Tax Reclaim Transaction Amount", "Report Entry Tax Reclaim Posted Amount",
    "Report Entry Tax Code", "Report Entry Tax Reclaim Domestic Flag", "Report Entry Tax Adjusted Amount",
    "Report Entry Tax Reclaim Adjusted Amount", "Report Entry Tax Reclaim Code",
    "Report Entry Tax Reclaim Trans Adjusted Code", "Report Entry Tax Allocation Reclaim Code",
    "Travel Request ID", "Travel Request Name", "Travel Request Total Posted Amount",
    "Travel Request Total Approved Amount", "Travel Request Start Date", "Travel Request End Date",
    "Travel Request Authorized Date", "Report Entry Total Tax Posted Amount", "Net Tax Amount",
    "Report Entry Total Reclaim Adjusted Amount", "Net Adjusted Reclaim Amount",
    "Report Entry Payment Type Name", "Card Program Type Code", "Statement Period Start Date",
    "Statement Period End Date", "Cash Account Code", "Liability Account Code", "Estimated Payment Date"
];

/*
var HEADERS = ["Date", "Purpose", "Concur Expense Report ID",
    "Line Item Date", "Line Item Category", "Line Item Description",
    "Line Item Location", "Line Item Department",
    "Line Item Currency", "Line Item Amount", "Line Item Exchange Rate", "Line Item Non Reimbursable"
];
*/

/*
HEADERS = ["Concur field", "Request Key",
                "Expense Report No", "Employee","Employee Code", "Transaction Date", "Purpose", "Advance To Apply",
                "Subsidiary", "Department", "Location", "Account", "Total Amount",
                "Line Item Amount", "Line Item Exchange Rate", "Line Item Description", "Line Item Expense Date",
              	"Line Item Foreign Amount", "Line Item Currency Code", "Line Item Category", "Line Item Department", "Request ID"];
*/
var LINE_INFO = ["Line Item Expense Date",
    "Line Item Category",
    "Line Item Description",
    "Line Item Location",
    "Line Item Department",
    "Line Item Currency",
    "Line Item Amount",
    "Line Item Exchange Rate",
    "Line Item Non Reimbursable"
];

function getFieldObjects(fields) {
    var fieldsObjects = {
        lineFields: [],
        headerFields: []
    };

    fields.forEach(function(field, i) {
        if (LINE_INFO.indexOf(HEADERS[i]) !== -1) {
            // console.log('line field :: '  + HEADERS[i]);
            fieldsObjects.lineFields.push(new Field({
                value: field,
                apiName: HEADERS[i],
                type: 'String'
            }));
        } else {
            /*
            if (config.invoicePrefix && HEADERS[i] === 'Expense Report No') {
                field = config.invoicePrefix + field;
                // console.log(field);
            }
            */
            fieldsObjects.headerFields.push(new Field({
                value: field,
                apiName: HEADERS[i],
                type: 'String'
            }));
        }
    });

    return fieldsObjects;
}

function getNewRecord(fieldsObjects) {
    return new ExpenseRecord({
        recordType: 'expensereport',
        flowName: 'Concur Expense Report Flow',
        fields: fieldsObjects.headerFields,
        childRecords: [new ChildRecord({
            fields: fieldsObjects.lineFields,
            childType: 'sublist',
            childIndex: 1,
            relationshipName: 'lineitem'
        })]
    });
}

function mergeJsonRecords(oldRecord, fieldsObjects) {
    //console.log('adding child');
    oldRecord.addChildRecord(new ChildRecord({
        fields: fieldsObjects.lineFields,
        childType: 'sublist',
        childIndex: (oldRecord.getChildRecords().length + 1),
        relationshipName: 'lineitem'
    }));
}

function getJsonRecords(expenseFile) {
    var deferred = Q.defer();
    var jsonRecordKeyToRecord = {},
        rows,
        records,
        lineFields,
        headerFields,
        fields;

    try {

        // Start, added by Mohit Kumar for PO same linenumber change on 13th April 2016
        // expenseFile = "D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\ExpenseData\\extract_CES_SAE_v2_p0072773kxrw_20160809211828.txt";
        console.log('expenseFile :' + expenseFile);
        // process.exit(0);
        // END, added by Mohit Kumar for PO same linenumber change on 13th April 2016

        fs.readFile(expenseFile, function(err, data) {
            if (err) {
                return deferred.reject(err.message);
            }

            data = data.toString();

            if (data.indexOf('EXTRACT') !== 0) {
                return deferred.reject('Invalid file content received from Concur - ' + data);
            }

            //console.log('removing line 1 ...');

            //remove 1st line
            data = data.replace(/[^\r\n]+\r\n/, '');
            rows = data.split('\r\n');
            //flush data
            data = '';

            console.log('processing rows ...');
            rows.forEach(function(row, i) {
                var fieldsObject;
                if (!row) {
                    return;
                }
                fields = row.split('|');
                fieldsObject = getFieldObjects(fields);
                if (jsonRecordKeyToRecord[fieldsObject.headerFields[4].getValue()]) {
                    mergeJsonRecords(jsonRecordKeyToRecord[fieldsObject.headerFields[4].getValue()], fieldsObject);
                } else {
                    jsonRecordKeyToRecord[fieldsObject.headerFields[4].getValue()] = getNewRecord(fieldsObject);
                }
                //console.log('processed row: ' + i)
            });

            records = _.map(_.values(jsonRecordKeyToRecord), function(record) {
                return record;
            });

            console.log('Total records : ' + records.length);

            /*if(config.maxRecords){
            	records = records.splice(config.maxRecords);
            }*/

            console.log('Total records : ' + records.length);

            if (_.isArray(config.testRequestKeys)) {
                records = _.filter(records, function(record) {
                    return config.testRequestKeys.indexOf(record.readField('Employee ID').getValue()) !== -1;
                });
            }

            console.log('Total records after test filter : ' + records.length);

            // Start, added DEBUG logs by Mohit Kumar for Expense Report on 18th May 2016
            /*
            SyncLogger.log({
              DEBUG: JSON.stringify(records),
              success: 0,
              errors: 0,
              flowType: process.argv[2]
            });
            process.exit(0);
            */
            // END, added DEBUG logs by Mohit Kumar for Expense Report on 18th May 2016

            _.each(records, function(jsonRecord) {
                jsonRecord.getChildRecords().forEach(function(childRec) {

                    if (jsonRecord.readField('Report Entry Vendor Name').getValue()) {
                        childRec.readField('Line Item Description').setValue(
                            jsonRecord.readField('Report Entry Vendor Name').getValue() + '::' +
                            childRec.readField('Line Item Description').getValue());
                    } else {
                        childRec.readField('Line Item Description').setValue(
                            childRec.readField('Line Item Description').getValue());
                    }
                });
            });

            return deferred.resolve(records);
        });
    } catch (ex) {
        return deferred.reject(ex.message);
    }

    return deferred.promise;
}

ExpenseRecordBuilder.getExpenseRecords = function(file) {
    return getJsonRecords(file);
};

module.exports = ExpenseRecordBuilder;
