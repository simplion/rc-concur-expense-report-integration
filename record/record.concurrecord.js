var util = require('util');
var _ = require('lodash');
var AbstractRecord = require('./record.abstractrecord');


function ConcurRecord(args) {
	
    ConcurRecord.super_.call(this, args);

    this.smplnClassName = 'record.concurrecord';

};

util.inherits(ConcurRecord, AbstractRecord);

ConcurRecord.prototype.toPlatformRecord = function () {
    throw new Error('Not yet implemented');
};


module.exports = ConcurRecord;