var util = require('util');
var _ = require('lodash');
var AbstractRecord = require('./record.abstractrecord');


function ChildRecord(args) {
	
    ChildRecord.super_.call(this, args);

    // childType could be childRelationships/relatedRecords for SF, sublist and subrecords for NS
    this._childType = args.childType;

    //maintained in SF for childRelationships
    this._relationshipName = args.relationshipName;

    //foreign key that maintains the relationship in the child record
    this._field = args.field;
    
    /** In certain cases its useful to reatin the childIndex
     *  We want to retain the order of subrecords/sublists so, that mapping is fair
     *  and to apply line item discount on SO its important that discount item is inserted at the correct place.
     **/
    this._childIndex = args.childIndex;

    this.smplnClassName = 'record.childrecord';

};

util.inherits(ChildRecord, AbstractRecord);

ChildRecord.prototype.getChildIndex = function () {
    return this._childIndex;
};

ChildRecord.prototype.setChildIndex = function (childIndex) {
    this._childIndex = childIndex;
};

ChildRecord.prototype.getChildType = function () {
    return this._childType;
};

ChildRecord.prototype.setChildType = function (childType) {
    this._childType = childType;
};

ChildRecord.prototype.getRelationshipName = function () {
    return this._relationshipName;
};

ChildRecord.prototype.setRelationshipName = function (relationshipName) {
    this._relationshipName = relationshipName;
};

ChildRecord.prototype.getField = function () {
    return this._field;
};

ChildRecord.prototype.setField = function (field) {
    this._field = field;
};

module.exports = ChildRecord;