var AbstractFlow = require('./flow.abstractflow');
var util = require('util');
var Q = require('Q');
var Mailer = require('../utils/utils.mailer');
var Logger = require('../utils/utils.logger');
var config = require('../config/config');

function BlacklineFlow(config){
	BlacklineFlow.super_.call(this, config);
	
	this.targetEndPointConnector = config.blacklineConnector;
	this.targetEndPointData = config.blacklineData;
	
	this.smplnClassName = 'flow.blacklineflow';
}

util.inherits(BlacklineFlow, AbstractFlow);

BlacklineFlow.prototype.getBlacklineConnector = function(){
	return this.getTargetEndPointConnector();
};

BlacklineFlow.prototype.setBlacklineConnector = function(blacklineConnector){
	this.setTargetEndPointConnector(blacklineConnector);
};

BlacklineFlow.prototype.getBlacklineData = function(){
	return this.getTargetEndPointData();
};

BlacklineFlow.prototype.setBlacklineData = function(blacklineData){
	this.setTargetEndPointData(blacklineData);
};

BlacklineFlow.prototype.run = function(){
	var deferred = Q.defer();
	var that = this;
	
	var dataRows = 0,
		ftpFile,
		errored = false;
	
	//get data from endpoint
	this.getEndPointConnector().readData(this.getEndPointData())
	.then(function(dataFile){
		console.log('data grabbed from endpoint successfully.');
		return that.getMapper().mapToBlackline(dataFile, that.getEndPointData());
	}, function(err){
		console.log('data NOT grabbed from endpoint');
		errored = true;
		return Mailer.sendMail(config.mailer.errorReceiver, 
				'Blackline Integration :: An error occurred while fetching file from ' + that.getEndPointConnector().getEndPointName(),
				'<br/>' + err + '<br/>');
	}).then(function(data){
		if(errored){
			return;
		}
		console.log('mapping done :: ' + data.rows);
		ftpFile = data.fileName;
		dataRows = data.rows;
		return that.getBlacklineConnector().upload(ftpFile, 
				that.getBlacklineData().getFolderPath() + (ftpFile.substring(ftpFile.lastIndexOf('\\') + 1)));
	}, function(err){
		if(errored){
			return;
		}
		console.log('mapping NOT done');
		errored = true;
		return Mailer.sendMail(config.mailer.errorReceiver, 
				'Blackline Integration :: An error occurred while performing mapping for ' + that.getEndPointConnector().getEndPointName(),
				'<br/>' + err + '<br/>');
	}).then(function(fileName){
		if(errored){
			return;
		}
		console.log('FTP Done');
		try{
			//send mail
			return Mailer.sendMail(config.mailer.blacklineAlertReceiver, 
				'Blackline Integration :: ' + that.getEndPointConnector().getEndPointName() + ' File uploaded',
				'A file ' + ftpFile + ' was uploaded successfully!<br/>' +
				'Rows uploaded :: ' + dataRows + '<br/>' +
				'Start Date :: ' + that.getEndPointData().getDataBeginDate() + '<br/>' + 
				'End Date :: ' + that.getEndPointData().getDataEndDate() + '<br/>');
		} catch(ex){
			console.log('Mailer errorred :: ' + ex.message);
		}
	}, function(err){
		if(errored){
			return;
		}
		errored = true;
		return Mailer.sendMail(config.mailer.errorReceiver, 
				'Blackline Integration :: An error occurred while Uploading ' + that.getEndPointConnector().getEndPointName() +' file to FTP',
				'<br/>File :: ' + ftpFile + '<br/>' +
				'<br/>' + err + '<br/>');
	}).then(function(){
		return Logger.log({
			count: dataRows,
			flowType: process.argv[2]
		});
	}, function(err){
		return deferred.reject(err);
	}).then(function(){
		return deferred.resolve();
	}, function(err){
		return deferred.reject(err);
	});
	
	return deferred.promise;
};

module.exports = BlacklineFlow;