var Serializable = require('../utils/utils.serializable');
var util = require('util');
var Q = require('Q');
var Mailer = require('../utils/utils.mailer');
var Logger = require('../utils/utils.logger');
var config = require('../config/config');

function AbstractFlow(config){
	AbstractFlow.super_.call(this, config);

	this.targetEndPointConnector = config.targetEndPointConnector;
	this.targetEndPointData = config.targetEndPointData;
	this.endPointConnector = config.endPointConnector;
	this.endPointData = config.endPointData;
	this.mapper = config.mapper;

	this.smplnClassName = 'flow.abstractflow';
}

util.inherits(AbstractFlow, Serializable);

AbstractFlow.prototype.getTargetEndPointConnector = function(){
	return this.targetEndPointConnector;
};

AbstractFlow.prototype.setTargetEndPointConnector = function(targetEndPointConnector){
	this.targetEndPointConnector = targetEndPointConnector;
};

AbstractFlow.prototype.getTargetEndPointData = function(){
	return this.targetEndPointData;
};

AbstractFlow.prototype.setTargetEndPointData = function(targetEndPointData){
	this.targetEndPointData = targetEndPointData;
};

AbstractFlow.prototype.getEndPointConnector = function(){
	return this.endPointConnector;
};

AbstractFlow.prototype.setEndPointConnector = function(endPointConnector){
	this.endPointConnector = endPointConnector;
};

AbstractFlow.prototype.getEndPointData = function(){
	return this.endPointData;
};

AbstractFlow.prototype.setEndPointData = function(endPointData){
	this.endPointData = endPointData;
};

AbstractFlow.prototype.getMapper = function(){
	return this.mapper;
};

AbstractFlow.prototype.setMapper = function(mapper){
	this.mapper = mapper;
};

AbstractFlow.prototype.run = function(){
	throw new Error('Abstract method run must be implemented');
};

module.exports = AbstractFlow;
