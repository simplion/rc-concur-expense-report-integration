var AbstractFlow = require('./flow.abstractflow');
var util = require('util');
var Q = require('Q');
var Mailer = require('../utils/utils.mailer');
var Logger = require('../utils/utils.logger');
var config = require('../config/config');
var ConcurRecordBuilder = require('../record/record.concurrecordbuilder');
var NetSuiteRestletData = require('../connector/data/connector.data.netsuiterestletdata');
var _ = require('lodash');
var fs = require('fs');
var BigNumber = require('big.js');
var pdfUtils = require('../utils/utils.pdf');

function ConcurFlow(config) {
    ConcurFlow.super_.call(this, config);

    this.targetEndPointConnector = config.concurApiConnector;
    this.targetEndPointData = config.concurData;

    this.smplnClassName = 'flow.concurflow';
}

util.inherits(ConcurFlow, AbstractFlow);

ConcurFlow.prototype.getConcurApiConnector = function() {
    return this.getEndPointConnector();
};

ConcurFlow.prototype.setConcurApiConnector = function(concurApiConnector) {
    this.setEndPointConnector(concurApiConnector);
};

ConcurFlow.prototype.getConcurData = function() {
    return this.getEndPointData();
};

ConcurFlow.prototype.setConcurData = function(concurData) {
    this.setEndPointData(concurData);
};

/**
 * This method returns the restletBodies
 * Ensures only one file split Job is running at a time - this is important as OS may not allow spawning multiple process
 * and each split job may spawn multiple process for splitting pdf attachments
 */
ConcurFlow.prototype.readRestletBodiesForFiles = function(successRecords) {
    var deferred = Q.defer();

    var restletBodies = [],
        errors = [],
        success = 0,
        imageFileNames = [],
        lock = false;

    var resolvePromiseAndReleaseLock = function() {
        if (success + errors.length >= successRecords.length) {
            deferred.resolve({
                restletBodies: restletBodies,
                errors: errors,
                imageFileNames: imageFileNames
            });
        }
        lock = false;
    };

    var splitFileAndComputeRestletBody = function(successRecord) {

        //Process only one split job at a time
        if (lock) {
            return setTimeout(function() {
                splitFileAndComputeRestletBody(successRecord);
            }, 500);
        }

        //Take a lock, ensuring only a single job is running
        lock = true;

        pdfUtils.getSplittedFiles(successRecord.readField('Request ID')).then(function(fileNames) {
            var files = [];
            try {
                //console.log('got file splits: ' + JSON.stringify(fileNames) + '. Success: ' + success);
                //Read all the files data and push into an Array
                _.each(fileNames, function(fileName) {
                    imageFileNames.push(fileName);
                });

                if (fileNames.length) {
                    //Upload all splits of a file in a single request
                    restletBodies.push(new NetSuiteRestletData({
                        url: config.concurRestletUrl,
                        httpMethod: 'POST',
                        record: successRecord,
                        body: {
                            pdffiles: fileNames,
                            fileNames: fileNames,
                            tranid: successRecord.readField('Vendor Invoice Number').getValue(),
                            vendorid: successRecord.readField('Vendor Code').getValue()
                        },
                    }));
                }
                success++;
                console.log('got file splits: ' + JSON.stringify(fileNames) + '. Success: ' + success);
                resolvePromiseAndReleaseLock();
            } catch (ex) {
                console.dir(ex);
                errors.push({
                    error: 'Failed to read file from local file system. Due to - ' + (ex.message || JSON.stringify(ex)),
                    record: successRecord
                });
                resolvePromiseAndReleaseLock();
            }
        }, function(err) {
            console.dir(err);
            errors.push({
                error: err,
                record: successRecord
            });
            resolvePromiseAndReleaseLock();
        });
    };

    if (successRecords.length >= 1) {
        _.each(successRecords, function(successRecord) {
            splitFileAndComputeRestletBody(successRecord);
        });
    } else {
        //Ensure promise is resolved for 0 successRecords as well
        setImmediate(function() {
            return deferred.resolve({
                restletBodies: restletBodies,
                errors: errors,
                imageFileNames: imageFileNames
            });
        });
    }

    return deferred.promise;
};

/**
 * Process each Job's file
 * Ideally there would be only one Job and one File to process
 * But, in certain scenarios there could be multiple files for a job, or multiple files and multiple jobs
 */
ConcurFlow.prototype.processFiles = function(fileNames, i, promise) {
    var deferred = promise || Q.defer(),
        errored = false,
        that = this,
        fileData,
        imageFileNames,
        //recordsToSync,
        successRecords,
        totalAmount = new BigNumber(0),
        concurData = this.getEndPointData(),
        fileName = fileNames[i];

    //all files processed
    if (i >= fileNames.length) {
        //console.log('all files processed')
        return deferred.resolve();
    } else {
        console.log('processing file ' + i + ' out of ' + fileNames.length);
    }

    try {
        console.log('reading file: ' + fileName);
        fileData = require('fs').readFileSync(fileName);
        //console.log(fileData);
        console.log(fileName.substring(fileName.lastIndexOf('\\') + 1));
    } catch (ex) {
        errored = true;
        //reject after a sec, so, that mail is sent
        return setTimeout(function() {
            deferred.reject({
                subject: 'Concur API Integration :: An error occurred while reading downloaded concur file ' + fileName,
                body: '<br/>' + err + '<br/>'
            });
        }, 1000);
    }

    //Upload the Job file into NetSuite
    console.log('Flow Step 4a: Uploading File');
    that.getTargetEndPointConnector().readData(new NetSuiteRestletData({
        url: config.concurRestletUrl,
        httpMethod: 'POST',
        body: JSON.stringify({
            "file": fileData.toString(),
            "fileName": fileName.substring(fileName.lastIndexOf('\\') + 1)
        })
    })).then(function(body) {

        if (errored) {
            return;
        }
        console.dir(body);

        try {
            body = JSON.parse(body);
        } catch (ex) {
            errored = true;
            return deferred.reject({
                subject: 'Concur API Integration :: An error occurred while Uploading file ' + fileName + ' to NetSuite',
                body: '<br/>' + (body || '') + '<br/>'
            });
        }

        if (body.status !== 'success') {
            errored = true;
            return deferred.reject({
                subject: 'Concur API Integration :: An error occurred while Uploading file ' + fileName + ' to NetSuite',
                body: '<br/>' + (body.error || '') + '<br/>'
            });
        }

        //After uploading file into NetSuite, compute ConcurReocrd from pipe separated file
        return ConcurRecordBuilder.getConcurRecords(fileName);

    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while Uploading file ' + fileName + ' to NetSuite',
            body: '<br/>' + err + '<br/>'
        });

    }).then(function(records) {
        var url = config.concurRestletUrl;

        if (errored) {
            return;
        }
        console.dir(records.length);
        //recordsToSync = records;

        //Prepare records to be uploaded
        var restletBodies = [];
        _.each(records, function(record) {
            var rec = JSON.stringify([record]);
            if (config.concurImageOnly) {
                rec = {
                    records: JSON.parse(rec)
                };
                rec.concurImageOnly = true;
                rec = JSON.stringify(rec);
            }

            //console.dir(rec);

            //console.log('totalAmount...' + totalAmount.toString());
            totalAmount = totalAmount.plus(parseFloat(record.readField('Total Amount').getValue()));
            restletBodies.push(new NetSuiteRestletData({
                url: url,
                httpMethod: 'POST',
                record: record,
                //if image only then extend
                body: rec,
                amount: parseFloat(record.readField('Total Amount').getValue())
            }));
        });

        //Upload records to NetSuite concurrently
        console.log('sending requests for amount of...' + totalAmount);
        return that.getTargetEndPointConnector().readUsingConcurrentRequests(restletBodies);
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.log('error building records');
        console.dir(err);
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while processing Extract file for Job ' + concurData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function(obj) {
        logStats = obj;
        console.log('fetching Images for successRecords...' + logStats.successRecords.length);
        if (errored) {
            return;
        }
        successRecords = obj.successRecords;
        //Download all images to local file system
        return that.getConcurApiConnector().getAllImages(obj.successRecords);
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while pushing data to NetSuite for Job ' + concurData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function() {
        //Compute Image files to be uploaded
        return that.readRestletBodiesForFiles(successRecords);
    }, function(err) {
        errored = true;
        if (_.isObject(err)) {
            err = err.message || JSON.stringify(err);
        }
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while fetching Images for Job ' + concurData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function(obj) {
        //Upload all images to NetSuite
        if (errored) {
            return;
        }
        console.log('Uploading Images...');
        var restletBodies = [],
            errors = [],
            success = 0;

        console.log('sending requests for Images...');
        //Upload all images to local file system
        imageFileNames = obj.imageFileNames;
        return that.getTargetEndPointConnector().readUsingConcurrentRequests(obj.restletBodies);
    }, function(err) {
        errored = true;
        if (_.isObject(err)) {
            err = err.message || JSON.stringify(err);
        }
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while preparing Images to upload in NetSuite, for Job ' + concurData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function(obj) {

        var errorAmount = new BigNumber(0);
        //logStats = obj;
        if (errored) {
            return;
        }
        try {
            //send mail

            // Start, added by Mohit Kumar for mail subject on 13th Jun 2016
            var mailSubject = 'Concur API Integration :: Invoice integration status',
                // END, added by Mohit Kumar for mail subject on 13th Jun 2016
                mailBody = 'Total amount in file (Regardless of Currency) :: ' + totalAmount.toString() + '<br/><br/>';

            //Add Job Details
            if (concurData.getJobDateTimes() && concurData.getJobDateTimes().length > 0) {

                // Start, added by Mohit Kumar for mail subject on 13th Jun 2016
                mailSubject += ' (' + concurData.getJobDateTimes()[0].format('MM-DD-YYYY') + ')';
                // END, added by Mohit Kumar for mail subject on 13th Jun 2016

                mailBody += 'Job Date :: ' + concurData.getJobDateTimes()[0].format('MM-DD-YYYY') + '<br/>';
            }

            if (concurData.getFileNames() && concurData.getFileNames().length > 0) {
                mailBody += 'Job File :: ' + concurData.getFileNames()[0] + '<br/><br/>';
            }

            //Add Invoice/Image Summary
            mailBody +=
                'Invoices created :: ' + logStats.success + '<br/>' +
                'Invoice Errored :: ' + logStats.errors.length + '<br/>' +
                'Images Uploaded :: ' + obj.success + '<br/>' +
                'Images Errored :: ' + obj.errors.length + '<br/>';

            if (logStats.errors.length > 0) {
                mailBody += '<br/><h3>Invoice Error details</h3><br/>';
                mailBody += '<table border="1" cellspacing="0" cellpadding="4">';
                mailBody += '<thead><tr style="font-weight: bold;"><td>Id</td><td>Error</td><td>Vendor</td><td>Vendor Invoice Number</td><td>PO#</td><td>Amount</td></tr></thead>';
                logStats.errors.forEach(function(errorMsg) {
                    totalAmount = totalAmount.minus((errorMsg.record.readField('Total Amount').getValue() || 0));
                    errorAmount = errorAmount.plus((errorMsg.record.readField('Total Amount').getValue() || 0));
                    /*mailBody += JSON.stringify({id: errorMsg.id, error: errorMsg.error,
                    	vendor: errorMsg.record.readField('Vendor Name').getValue(),
                    	"Invoice Number": errorMsg.record.readField('Vendor Invoice Number').getValue(),
                    	"Amount": errorMsg.record.readField('Total Amount').getValue()}) + '<br/>';*/

                    if (_.isObject(errorMsg.error)) {
                        errorMsg.error = JSON.stringify(errorMsg.error);
                    }

                    mailBody += '<tr>' +
                        '<td>' + errorMsg.id + '</td>' +
                        '<td>' + errorMsg.error + '</td>' +
                        '<td>' + errorMsg.record.readField('Vendor Name').getValue() + '</td>' +
                        '<td>' + errorMsg.record.readField('Vendor Invoice Number').getValue() + '</td>' +
                        '<td>' + (errorMsg.record.readField('PO Number') ?
                            errorMsg.record.readField('PO Number').getValue() : '') + '</td>' +
                        '<td>' + errorMsg.record.readField('Total Amount').getValue() + '</td>' +
                        '</tr>';
                });
                mailBody += '<tr><td colspan="5">Total<td>' + errorAmount.toString() + '</td></tr>';
                mailBody += '</table>';
            }

            errorAmount = new BigNumber(0);
            if (obj.errors.length > 0) {
                mailBody += '<br/><h3>Image Error details</h3><br/>';
                mailBody += '<table border="1" cellspacing="0" cellpadding="4">';
                mailBody += '<thead><tr style="font-weight: bold;"><td>Id</td><td>Error</td><td>Vendor</td><td>Vendor Invoice Number</td><td>PO#</td><td>Amount</td></tr></thead>';
                obj.errors.forEach(function(errorMsg) {
                    errorAmount = errorAmount.plus((errorMsg.record.readField('Total Amount').getValue() || 0));
                    mailBody += '<tr>' +
                        '<td>' + errorMsg.record.readField('Request Key').getValue() + '</td>' +
                        '<td>' + errorMsg.error + '</td>' +
                        '<td>' + errorMsg.record.readField('Vendor Name').getValue() + '</td>' +
                        '<td>' + errorMsg.record.readField('Vendor Invoice Number').getValue() + '</td>' +
                        '<td>' + (errorMsg.record.readField('PO Number') ?
                            errorMsg.record.readField('PO Number').getValue() : '') + '</td>' +
                        '<td>' + errorMsg.record.readField('Total Amount').getValue() + '</td>' +
                        '</tr>';
                });
                mailBody += '<tr><td colspan="5">Total<td>' + errorAmount.toString() + '</td></tr>';
                mailBody += '</table>';
            }

            //mailBody += '<br/>Total amount Created (Regardless of Currency) :: ' + totalAmount.toString();
            //console.log('success records: ' + logStats.successRecords.length);
            if (logStats.successRecords.length > 0) {
                mailBody += '<br/><h3>Created Invoice details<b></h3>';
                mailBody += '<table border="1" cellspacing="0" cellpadding="4">';
                mailBody += '<thead><tr style="font-weight: bold;"><td>Id</td><td>Vendor</td><td>Vendor Invoice Number</td><td>PO#</td><td>Overbilled</td><td>Amount</td></tr></thead>';
                _.each(logStats.successRecords, function(record, i) {

                    mailBody += '<tr>' +
                        '<td>' + record.readField('Request Key').getValue() + '</td>' +
                        '<td>' + record.readField('Vendor Name').getValue() + '</td>' +
                        '<td>' + record.readField('Vendor Invoice Number').getValue() + '</td>' +
                        '<td>' + (record.readField('PO Number') ?
                            record.readField('PO Number').getValue() : '') + '</td>' +
                        '<td>' + (logStats.overbilled[i] || 'No') + '</td>' +
                        '<td>' + record.readField('Total Amount').getValue() + '</td>' +
                        '</tr>';
                });
                mailBody += '<tr><td colspan="5">Total</td><td>' + totalAmount.toString() + '</td></tr>';
                mailBody += '</table>';
            }

            _.each(imageFileNames, function(imageFileName) {
                /** Delete Files **/
                try {
                    fs.unlinkSync(config.dataLocation + imageFileName);
                } catch (e) {
                    console.log('failed to delete file:' + (config.dataLocation + imageFileName));
                    console.dir(e);
                }
            });

            return Mailer.sendMail(config.mailer.alertReceiver,
                mailSubject, mailBody, {
                    filename: concurData.getFileNames()[0],
                    path: config.concurDataLocation + concurData.getFileNames()[0]
                });
        } catch (ex) {
            console.log('Mailer errorred :: ' + ex.message);
        }
        console.dir(obj);
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while pushing Image files to NetSuite for Job ' + concurData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function() {
        if (errored) {
            return;
        }
        console.log('mail sent');
        return Logger.log({
            success: logStats.success,
            errors: logStats.errors.length,
            flowType: process.argv[2]
        });
    }, function(err) {
        if (errored) {
            return;
        }

        if (_.isObject(err)) {
            err = err.message || JSON.stringify(err);
        }

        errored = true;

        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while Updating Log files',
            body: '<br/>' + err + '<br/>'
        });

    }).then(function() {
        if (errored) {
            return;
        }
        console.log('Logged');
        return that.processFiles(fileNames, i + 1, deferred);
    }, function(err) {
        if (errored) {
            return;
        }
        console.dir(err);
        return deferred.reject({
            subject: 'Concur API Integration :: An error occurred while logging',
            body: '<br/>' + err + '<br/>'
        });
    });

    return deferred.promise;
};

ConcurFlow.prototype.run = function() {
    var deferred = Q.defer();
    var that = this;

    var errored = false,
        logStats,
        fileName;

    var concurData = this.getEndPointData();

    console.log('Flow Step 1: Reading Extract Id');
    this.getConcurApiConnector().readExtractId(concurData).then(function() {
        console.log('Flow Step 2: Reading Job');
        return that.getConcurApiConnector().readLastDaysJob(concurData);
    }, function(err) {
        errored = true;
        console.dir(err);
        return Mailer.sendMail(config.mailer.errorReceiver,
            'Concur API Integration :: An error occurred while reading Extract id for ' + concurData.getExtract(),
            '<br/>' + err + '<br/>');
    }).then(function(fileId) {
        if (errored) {
            return;
        }
        console.log('Flow Step 3: Reading Extract File (Id) : ' + fileId);
        //console.dir(fileId);
        // Start, added by Mohit Kumar for testing
        // that.getConcurApiConnector().readExtractFile(concurData);
        // process.exit(0);
        // END, added by Mohit Kumar for testing
        return that.getConcurApiConnector().readExtractFile(concurData);
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        if (_.isObject(err)) {
            err = err.message || JSON.stringify(err);
        }
        return Mailer.sendMail(config.mailer.errorReceiver,
            'Concur API Integration :: An error occurred processing Job ' + (concurData.getJobId() || '[Id Not yet determined]'),
            '<br/>' + err + '<br/>');
    }).then(function(files) {
        if (errored) {
            return;
        }

        //for each file
        return that.processFiles(files, 0);
    }, function(err) {
        if (errored) {
            return;
        }

        errored = true;
        console.dir(err);
    }).then(function() {
        if (errored) {
            return;
        }
        console.log('all done');
        return deferred.resolve();
    }, function(err) {
        if (errored) {
            return;
        }
        console.dir(err);
        //Send and reject
        Mailer.sendMail(config.mailer.errorReceiver,
            err.subject,
            err.body).then(function() {
            return deferred.reject();
        });
    });

    return deferred.promise;
};

module.exports = ConcurFlow;
