var AbstractFlow = require('./flow.abstractflow');
var util = require('util');
var Q = require('Q');
var Mailer = require('../utils/utils.mailer');
var Logger = require('../utils/utils.logger');
var config = require('../config/config');
var ExpenseRecordBuilder = require('../record/record.expenserecordbuilder');
var NetSuiteRestletData = require('../connector/data/connector.data.netsuiterestletdata');
var _ = require('lodash');
var fs = require('fs');
var AdmZip = require('adm-zip');
var BigNumber = require('big.js');
var pdfUtils = require('../utils/utils.pdf');

var SyncLogger = require('../utils/utils.synclogger');

function ExpenseFlow(config) {
    ExpenseFlow.super_.call(this, config);

    this.targetEndPointConnector = config.expenseApiConnector;
    this.targetEndPointData = config.expenseData;

    this.smplnClassName = 'flow.expenseflow';
}

util.inherits(ExpenseFlow, AbstractFlow);

ExpenseFlow.prototype.getExpenseApiConnector = function() {
    return this.getEndPointConnector();
};

ExpenseFlow.prototype.setExpenseApiConnector = function(expenseApiConnector) {
    this.setEndPointConnector(expenseApiConnector);
};

ExpenseFlow.prototype.getExpenseData = function() {
    return this.getEndPointData();
};

ExpenseFlow.prototype.setExpenseData = function(expenseData) {
    this.setEndPointData(expenseData);
};

/**
 * This method returns the restletBodies
 * Ensures only one file split Job is running at a time - this is important as OS may not allow spawning multiple process
 * and each split job may spawn multiple process for splitting pdf attachments
 */
ExpenseFlow.prototype.readRestletBodiesForFiles = function(successRecords) {
    var deferred = Q.defer();

    var restletBodies = [],
        errors = [],
        success = 0,
        imageFileNames = [],
        lock = false;

    var resolvePromiseAndReleaseLock = function() {
        if (success + errors.length >= successRecords.length) {
            deferred.resolve({
                restletBodies: restletBodies,
                errors: errors,
                imageFileNames: imageFileNames
            });
        }
        lock = false;
    };

    var splitFileAndComputeRestletBody = function(successRecord) {

        //Process only one split job at a time
        if (lock) {
            return setTimeout(function() {
                splitFileAndComputeRestletBody(successRecord);
            }, 500);
        }

        //Take a lock, ensuring only a single job is running
        lock = true;

        pdfUtils.getSplittedFiles(successRecord.readField('Request ID')).then(function(fileNames) {
            var files = [];
            try {
                //console.log('got file splits: ' + JSON.stringify(fileNames) + '. Success: ' + success);
                //Read all the files data and push into an Array
                _.each(fileNames, function(fileName) {
                    imageFileNames.push(fileName);
                });

                console.log('Sending Files ::');

                if (fileNames.length) {
                    //Upload all splits of a file in a single request
                    restletBodies.push(new NetSuiteRestletData({
                        url: config.expenseRestletUrl,
                        httpMethod: 'POST',
                        record: successRecord,
                        body: {
                            recordType: 'expensereport',
                            pdffiles: fileNames,
                            fileNames: fileNames,
                            tranid: successRecord.readField('Concur Expense Report ID').getValue(),
                            vendorid: successRecord.readField('Vendor Code').getValue()
                        },
                    }));
                }
                success++;
                console.log('got file splits: ' + JSON.stringify(fileNames) + '. Success: ' + success);
                resolvePromiseAndReleaseLock();
            } catch (ex) {
                console.dir(ex);
                errors.push({
                    error: 'Failed to read file from local file system. Due to - ' + (ex.message || JSON.stringify(ex)),
                    record: successRecord
                });
                resolvePromiseAndReleaseLock();
            }
        }, function(err) {
            console.dir(err);
            errors.push({
                error: err,
                record: successRecord
            });
            resolvePromiseAndReleaseLock();
        });
    };

    if (successRecords.length >= 1) {
        _.each(successRecords, function(successRecord) {
            splitFileAndComputeRestletBody(successRecord);
        });
    } else {
        //Ensure promise is resolved for 0 successRecords as well
        setImmediate(function() {
            return deferred.resolve({
                restletBodies: restletBodies,
                errors: errors,
                imageFileNames: imageFileNames
            });
        });
    }

    return deferred.promise;
};

/**
 * Process each Job's file
 * Ideally there would be only one Job and one File to process
 * But, in certain scenarios there could be multiple files for a job, or multiple files and multiple jobs
 */
ExpenseFlow.prototype.processFiles = function(fileNames, i, promise) {
    var deferred = promise || Q.defer(),
        errored = false,
        that = this,
        fileData,
        imageFileNames,
        //recordsToSync,
        successRecords,
        totalAmount = new BigNumber(0),
        expenseData = this.getEndPointData(),
        fileName = fileNames[i];

    //all files processed
    if (i >= fileNames.length) {
        //console.log('all files processed')
        return deferred.resolve();
    } else {
        console.log('processing file ' + i + ' out of ' + fileNames.length);
    }

    try {
        console.log('reading file: ' + fileName);

        // Start, added by Mohit Kumar for zipped file on 10th Aug 2016
        // console.log('-----: Start Extracting Zipped File:-----');
        if (fileName.endsWith('.zip')) {
            // reading archives
            var zip = new AdmZip(fileName);
            var zipEntries = zip.getEntries(); // an array of ZipEntry records

            fileName = fileName.substring(0, fileName.lastIndexOf('.zip'));

            zip.extractAllTo( /*target path*/ fileName, /*overwrite*/ true);

            zipEntries.forEach(function(zipEntry) {
                // console.log(zipEntry.toString()); // outputs zip entries information
                if (!zipEntry.isDirectory && zipEntry.name.search('extract_CES_SAE_v2_p0072773kxrw_') !== -1) {
                    var filePaths = null;
                    if (expenseData.getFilePaths()) {
                        filePaths = expenseData.getFilePaths();
                    } else {
                        filePaths = [];
                    }

                    fileName = fileName + '\\' + zipEntry.name;

                    filePaths.push(fileName);
                    expenseData.setFilePaths(filePaths);
                }
            });
            console.log('Extracted File Names ::' + expenseData.getFilePaths());
        }
        // console.log('-----: END Extracting Zipped File:-----');
        // process.exit(0);
        // END, added by Mohit Kumar for zipped file on 10th Aug 2016
        fileData = require('fs').readFileSync(fileName);
        // console.log(fileData);
        console.log(fileName.substring(fileName.lastIndexOf('\\') + 1));
    } catch (ex) {
        errored = true;
        //reject after a sec, so, that mail is sent
        return setTimeout(function() {
            deferred.reject({
                subject: 'Expense API Integration :: An error occurred while reading downloaded expense file ' + fileName,
                body: '<br/>' + err + '<br/>'
            });
        }, 1000);
    }

    //Upload the Job file into NetSuite
    console.log('Flow Step 4a: Uploading File');
    that.getTargetEndPointConnector().readData(new NetSuiteRestletData({
        url: config.expenseRestletUrl,
        httpMethod: 'POST',
        body: JSON.stringify({
            "recordType": 'expensereport',
            "file": fileData.toString(),
            "fileName": fileName.substring(fileName.lastIndexOf('\\') + 1)
        })
    })).then(function(body) {

        if (errored) {
            return;
        }
        // console.log('body :');
        console.dir(body);

        try {
            body = JSON.parse(body);
        } catch (ex) {
            errored = true;
            return deferred.reject({
                subject: 'Expense API Integration :: An error occurred while Uploading file ' + fileName + ' to NetSuite',
                body: '<br/>' + (body || '') + '<br/>'
            });
        }

        if (body.status !== 'success') {
            errored = true;
            return deferred.reject({
                subject: 'Expense API Integration :: An error occurred while Uploading file ' + fileName + ' to NetSuite',
                body: '<br/>' + (body.error || '') + '<br/>'
            });
        }

        //After uploading file into NetSuite, compute ExpenseReocrd from pipe separated file
        return ExpenseRecordBuilder.getExpenseRecords(fileName);

    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        return deferred.reject({
            subject: 'Expense API Integration :: An error occurred while Uploading file ' + fileName + ' to NetSuite',
            body: '<br/>' + err + '<br/>'
        });

    }).then(function(records) {
        var url = config.expenseRestletUrl;

        if (errored) {
            return;
        }

        console.dir(records.length);
        //recordsToSync = records;

        //Prepare records to be uploaded
        var restletBodies = [];
        _.each(records, function(record) {
            var rec = JSON.stringify([record]);
            if (config.expenseImageOnly) {
                rec = {
                    records: JSON.parse(rec)
                };
                rec.expenseImageOnly = true;
                rec = JSON.stringify(rec);
            }

            //console.dir(rec);

            // console.log('totalAmount...' + totalAmount.toString());
            // console.log('Employee 1st ::' + record.readField('Employee First Name').getValue());
            // console.log('Report Total Approved Amount ::' + record.readField('Report Total Approved Amount').getValue());
            totalAmount = totalAmount.plus(parseFloat(record.readField('Report Total Approved Amount').getValue()));

            // console.log('<--- record ---> ::' + JSON.stringify(record));
            // process.exit(0);
            restletBodies.push(new NetSuiteRestletData({
                url: url,
                httpMethod: 'POST',
                record: record,
                //if image only then extend
                body: rec,
                amount: parseFloat(record.readField('Report Total Approved Amount').getValue())
            }));
        });

        //Upload records to NetSuite concurrently
        console.log('sending requests for amount of...' + totalAmount);
        // process.exit(0);
        return that.getTargetEndPointConnector().readUsingConcurrentRequests(restletBodies);
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.log('error building records');
        console.dir(err);
        return deferred.reject({
            subject: 'Expense API Integration :: An error occurred while processing Extract file for Job ' + expenseData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function(obj) {
        logStats = obj;
        if (errored) {
            return;
        }
        successRecords = obj.successRecords;

        // Start, added by Mohit Kumar for Expense Report Mail on 29th August 2016
        var errorAmount = new BigNumber(0);
        // Start, un-commented by Mohit Kumar for Expense Report on 23rd August 2016
        if (errored) {
            return;
        }
        try {
            //send mail

            // Start, added by Mohit Kumar for mail subject on 13th Jun 2016
            var mailSubject = 'Expense API Integration :: Expense Report integration status',
                // END, added by Mohit Kumar for mail subject on 13th Jun 2016
                mailBody = 'Total amount in file (Regardless of Currency) :: ' + totalAmount.toString() + '<br/><br/>';

            //Add Job Details
            if (expenseData.getJobDateTimes() && expenseData.getJobDateTimes().length > 0) {

                // Start, added by Mohit Kumar for mail subject on 13th Jun 2016
                mailSubject += ' (' + expenseData.getJobDateTimes()[0].format('MMM Do YYYY') + ')';
                // END, added by Mohit Kumar for mail subject on 13th Jun 2016

                mailBody += 'Job Date :: ' + expenseData.getJobDateTimes()[0].format('MM-DD-YYYY') + '<br/>';
            }

            if (expenseData.getFileNames() && expenseData.getFileNames().length > 0) {
                mailBody += 'Job File :: ' + expenseData.getFileNames()[0] + '<br/><br/>';
            }

            //Add Invoice/Image Summary
            mailBody +=
                'Expense Report Created :: ' + logStats.success + '<br/>' +
                'Expense Report Errored :: ' + logStats.errors.length + '<br/>';

            if (logStats.errors.length > 0) {
                mailBody += '<br/><h3>Expense Report Error details</h3><br/>';
                mailBody += '<table border="1" cellspacing="0" cellpadding="4">';
                mailBody += '<thead><tr style="font-weight: bold;"><td>Id</td><td>Error</td><td>Employee</td><td>Expense Report No</td><td>Amount</td></tr></thead>';
                logStats.errors.forEach(function(errorMsg) {
                    totalAmount = totalAmount.minus((errorMsg.record.readField('Report Total Approved Amount').getValue() || 0));
                    errorAmount = errorAmount.plus((errorMsg.record.readField('Report Total Approved Amount').getValue() || 0));

                    if (_.isObject(errorMsg.error)) {
                        errorMsg.error = JSON.stringify(errorMsg.error);
                    }

                    mailBody += '<tr>' +
                        '<td>' + errorMsg.id + '</td>' +
                        '<td>' + errorMsg.error + '</td>' +
                        '<td>' + errorMsg.record.readField('Employee First Name').getValue() + '</td>' +
                        '<td>' + errorMsg.record.readField('Concur Expense Report ID').getValue() + '</td>' +
                        '<td>' + errorMsg.record.readField('Report Total Approved Amount').getValue() + '</td>' +
                        '</tr>';
                });
                mailBody += '<tr><td colspan="4">Total<td>' + errorAmount.toString() + '</td></tr>';
                mailBody += '</table>';
            }

            //mailBody += '<br/>Total amount Created (Regardless of Currency) :: ' + totalAmount.toString();
            //console.log('success records: ' + logStats.successRecords.length);
            if (logStats.successRecords.length > 0) {
                mailBody += '<br/><h3>Created Expense Report details<b></h3>';
                mailBody += '<table border="1" cellspacing="0" cellpadding="4">';
                mailBody += '<thead><tr style="font-weight: bold;"><td>Id</td><td>Employee</td><td>Expense Report No</td><td>Amount</td></tr></thead>';
                _.each(logStats.successRecords, function(record, i) {

                    mailBody += '<tr>' +
                        '<td>' + record.readField('Employee ID').getValue() + '</td>' +
                        '<td>' + record.readField('Employee First Name').getValue() + '</td>' +
                        '<td>' + record.readField('Concur Expense Report ID').getValue() + '</td>' +
                        '<td>' + record.readField('Report Total Approved Amount').getValue() + '</td>' +
                        '</tr>';
                });
                mailBody += '<tr><td colspan="3">Total</td><td>' + totalAmount.toString() + '</td></tr>';
                mailBody += '</table>';
            }

            return Mailer.sendMail(config.mailer.alertReceiver,
                mailSubject, mailBody, {
                    filename: expenseData.getFileNames()[0],
                    path: config.expenseDataLocation + expenseData.getFileNames()[0]
                });
        } catch (ex) {
            console.log('Mailer errorred :: ' + ex.message);
        }
        console.dir(obj);
        // END, added by Mohit Kumar for Expense Report Mail on 29th August 2016
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        return deferred.reject({
            subject: 'Expense API Integration :: An error occurred while pushing data to NetSuite for Job ' + expenseData.getJobId(),
            body: '<br/>' + err + '<br/>'
        });
    }).then(function() {
        if (errored) {
            return;
        }
        console.log('mail sent');
        return Logger.log({
            success: logStats.success,
            errors: logStats.errors.length,
            flowType: process.argv[2]
        });
    }, function(err) {
        if (errored) {
            return;
        }

        if (_.isObject(err)) {
            err = err.message || JSON.stringify(err);
        }

        errored = true;

        return deferred.reject({
            subject: 'Expense API Integration :: An error occurred while Updating Log files',
            body: '<br/>' + err + '<br/>'
        });

    }).then(function() {
        if (errored) {
            return;
        }
        console.log('Logged');
        return that.processFiles(fileNames, i + 1, deferred);
    }, function(err) {
        if (errored) {
            return;
        }
        console.dir(err);
        return deferred.reject({
            subject: 'Expense API Integration :: An error occurred while logging',
            body: '<br/>' + err + '<br/>'
        });
    });

    return deferred.promise;
};

ExpenseFlow.prototype.run = function() {
    var deferred = Q.defer();
    var that = this;

    var errored = false,
        logStats,
        fileName;

    var expenseData = this.getEndPointData();

    console.log('Flow Step 1: Reading Extract Id');
    this.getExpenseApiConnector().readExtractId(expenseData).then(function() {
        console.log('Flow Step 2: Reading Job');
        return that.getExpenseApiConnector().readLastDaysJob(expenseData);
    }, function(err) {
        errored = true;
        console.dir(err);
        return Mailer.sendMail(config.mailer.errorReceiver,
            'Expense API Integration :: An error occurred while reading Extract id for ' + expenseData.getExtract(),
            '<br/>' + err + '<br/>');
    }).then(function(fileId) {
        if (errored) {
            return;
        }
        console.log('Flow Step 3: Reading Extract File (Id) : ' + fileId);
        // Start, added by Mohit Kumar for Expense Report on 18th May 2016
        // console.dir(fileId);
        // process.exit(0);
        // END, added by Mohit Kumar for Expense Report on 18th May 2016
        return that.getExpenseApiConnector().readExtractFile(expenseData);
    }, function(err) {
        if (errored) {
            return;
        }
        errored = true;
        console.dir(err);
        if (_.isObject(err)) {
            err = err.message || JSON.stringify(err);
        }
        return Mailer.sendMail(config.mailer.errorReceiver,
            'Expense API Integration :: An error occurred processing Job ' + (expenseData.getJobId() || '[Id Not yet determined]'),
            '<br/>' + err + '<br/>');
    }).then(function(files) {
        if (errored) {
            return;
        }

        //for each file
        return that.processFiles(files, 0);
    }, function(err) {
        if (errored) {
            return;
        }

        errored = true;
        console.dir(err);
    }).then(function() {
        if (errored) {
            return;
        }
        console.log('all done');
        return deferred.resolve();
    }, function(err) {
        if (errored) {
            return;
        }
        console.dir(err);
        //Send and reject
        Mailer.sendMail(config.mailer.errorReceiver,
            err.subject,
            err.body).then(function() {
            return deferred.reject();
        });
    });

    return deferred.promise;
};

module.exports = ExpenseFlow;
