var _ = require('lodash');

var env;

if(process.argv.indexOf('-e') !== -1){
	env = process.argv[process.argv.indexOf('-e') + 1];
}
console.log("env: "  + (env || process.env.NODE_ENV || 'development'));

module.exports = _.extend(
	require('./env/all'),
	require('./env/' + (env || process.env.NODE_ENV || 'development')) || {}
);
