var config = require('../config');
module.exports = {
	smplnClassName: 'flow.concurflow',
	concurApiConnector: config.concurApiConnector,
	concurData: config.concurData,
	targetEndPointConnector: config.netSuiteRestlet
};