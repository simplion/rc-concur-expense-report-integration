var config = require('../config');
module.exports = {
	smplnClassName: 'flow.expenseflow',
	expenseApiConnector: config.expenseApiConnector,
	expenseData: config.expenseData,
	targetEndPointConnector: config.netSuiteRestlet
};
