var config = require('../config');
module.exports = {
	smplnClassName: 'flow.blacklineflow',
	blacklineConnector: config.blacklineConnector,
	blacklineData: config.blacklineNetSuiteData,
	endPointConnector: config.netSuiteOdbcConnector,
	endPointData: config.netsuiteData,
	mapper:{
		smplnClassName: 'mapper.netsuiteodbcmapper',
		mapping: {
			smplnClassName: 'mapper.mapping',
			fieldMappings: [
			  {sourceField: "InternalID", targetField: "InternalID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "period", targetField: "period", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "PostedDate", targetField: "PostedDate", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "AccountName", targetField: "AccountName", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "AccountNumber", targetField: "AccountNumber", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "TransactionHistoryID", targetField: "TransactionHistoryID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "UserID", targetField: "UserID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "TranID", targetField: "TranID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "CustomerID", targetField: "CustomerID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "CustomerName", targetField: "CustomerName", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Billingtransactionid", targetField: "Billingtransactionid", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "ItemName", targetField: "ItemName", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "quantity", targetField: "quantity", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "ItemRate", targetField: "ItemRate", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "amount", targetField: "amount", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "ExchangeRate", targetField: "ExchangeRate", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "AmountForeign", targetField: "AmountForeign", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "CURRENCY", targetField: "CURRENCY", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Type", targetField: "Type", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "source", targetField: "source", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "PaySystem", targetField: "PaySystem", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "CCType", targetField: "CCType", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "RCTransactionType", targetField: "RCTransactionType", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "memo", targetField: "memo", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "IncomeAccount", targetField: "IncomeAccount", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "DeferredRevAccount", targetField: "DeferredRevAccount", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "DateCreated", targetField: "DateCreated", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "CreatedBy", targetField: "CreatedBy", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "LastModifiedDate", targetField: "LastModifiedDate", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Subsidiary", targetField: "Subsidiary", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "SubsidiaryID", targetField: "SubsidiaryID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Duration", targetField: "Duration", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "RCRefundCategory", targetField: "RCRefundCategory", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "ScheduleID", targetField: "ScheduleID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "RevRecStart", targetField: "RevRecStart", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "RevRecEnd", targetField: "RevRecEnd", smplnClassName: 'mapper.fieldmapping'}
		  ]
		}
	}
};