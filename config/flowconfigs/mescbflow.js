var config = require('../config');
module.exports = {
	smplnClassName: 'flow.blacklineflow',
	blacklineConnector: config.blacklineConnector,
	blacklineData: config.blacklineMesCbData,
	endPointConnector: config.mesHttpConnector,
	endPointData: config.mesCbData,
	mapper:{
		smplnClassName: 'mapper.mestoftpmapper',
		mapping: {
			smplnClassName: 'mapper.mapping',
			fieldMappings: [
			  {sourceField: "Merchant Id", targetField: "Merchant Id", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "DBA Name", targetField: "DBA Name", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Control Number", targetField: "Control Number", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Incoming Date", targetField: "Incoming Date", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Card Number", targetField: "Card Number", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Reference Number", targetField: "Reference Number", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Tran Date", targetField: "Tran Date", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Tran Amount", targetField: "Tran Amount", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Purchase ID", targetField: "Purchase ID", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Auth Code", targetField: "Auth Code", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Adj Date", targetField: "Adj Date", smplnClassName: 'mapper.fieldmapping', 
				  transformer: function(sourceValue, sourceJson, fieldMappings){
					  if(sourceValue){
						  return sourceValue;
					  }
					  //Copy value from Incoming Date
					  return sourceJson[fieldMappings[3].getSourceField()];
				  }
			  },
			  {sourceField: "Adj Ref Num", targetField: "Adj Ref Num", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Reason", targetField: "Reason", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "First Time", targetField: "First Time", smplnClassName: 'mapper.fieldmapping'},			  
			  {sourceField: "Reason Code", targetField: "Reason Code", smplnClassName: 'mapper.fieldmapping'},	
			  {sourceField: "CB Ref Num", targetField: "CB Ref Num", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Terminal ID", targetField: "Terminal ID", smplnClassName: 'mapper.fieldmapping'}
		  ]
		}
	}
};