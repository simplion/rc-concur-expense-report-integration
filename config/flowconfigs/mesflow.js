var config = require('../config');
var moment = require('moment');
module.exports = {
	smplnClassName: 'flow.blacklineflow',
	blacklineConnector: config.blacklineConnector,
	blacklineData: config.blacklineMesData,
	endPointConnector: config.mesHttpConnector,
	endPointData: config.mesData,
	mapper:{
		smplnClassName: 'mapper.mestoftpmapper',
		mapping: {
			smplnClassName: 'mapper.mapping',
			fieldMappings: [
			  {sourceField: "Merchant Id", targetField: "Merchant Id", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "DBA Name", targetField: "DBA Name", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Term Num", targetField: "Term Num", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Batch Num", targetField: "Batch Num", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Batch Date", targetField: "Batch Date", smplnClassName: 'mapper.fieldmapping', 
				  transformer: function(sourceValue){
					  if(!sourceValue){return;}
					  return moment(sourceValue, 'MM/DD/YYYY').add(-1, 'days').format('MM/DD/YYYY');
				  }
			  },
			  {sourceField: "Tran Date", targetField: "Tran Date", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Card Type", targetField: "Card Type", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Card Number", targetField: "Card Number", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Reference", targetField: "Reference", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Purchase Id", targetField: "Purchase Id", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Client Reference", targetField: "Client Reference", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Auth Code", targetField: "Auth Code", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Entry Mode", targetField: "Entry Mode", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Tran Amount", targetField: "Tran Amount", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Currency Code", targetField: "Currency Code", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Original Amount", targetField: "Original Amount", smplnClassName: 'mapper.fieldmapping'},
			  {sourceField: "Original Currency Code", targetField: "Original Currency Code", smplnClassName: 'mapper.fieldmapping'}
		  ]
		}
	}
};