/* jshint node: true */
'use strict';
//
var moment = require('moment'),
	startTime = null,
	endTime = null;

if(process.argv[3] && process.argv[3].indexOf('-') !== 0){
	startTime = moment(process.argv[3], 'YYYYMMDD');
	//console.log('startTime : ' + startTime instanceof moment);
}

if(process.argv[4] && process.argv[4].indexOf('-') !== 0){
	endTime = moment(process.argv[4], 'YYYYMMDD');
	//console.log('endTime : ' + endTime);
}

module.exports = {
	concurImageOnly: process.argv.indexOf('--image') !== -1,
	expenseImageOnly: process.argv.indexOf('--image') !== -1,
	concurApiConnector: {
		oauthToken: '0_ykP4tGoSCATWXOnr0fTk+tLAs=',
		smplnClassName: 'connector.concurapiconnector'
	},
	expenseApiConnector: {
		oauthToken: '0_ykP4tGoSCATWXOnr0fTk+tLAs=',
		smplnClassName: 'connector.expenseapiconnector'
	},
	blacklineConnector: {
		host:'ftp.blackline.com',
		username: 'RingCentral',
		password: '7NBWzdMP',
		port: '22',
		smplnClassName: 'connector.blacklineconnector'
	},
	mesHttpConnector: {
	//	host:'https://www.merchante-solutions.com/jsp/reports/report_api.jsp',
		userId: 'francis.lee@ringcentral.com',
		userPass: 'Toshiba1!',
		smplnClassName: 'connector.meshttpconnector'
	},
	concurData: {
		extract: 'Standard Payment Request Accounting Extract v2.1 (ONP)',
		smplnClassName: 'connector.data.concurdata',
		startTime: startTime,
		endTime: endTime
	},
	expenseData: {
		extract: 'AP/GL Extract V.2.4.4',
		smplnClassName: 'connector.data.expensedata',
		startTime: startTime,
		endTime: endTime
	},
	blacklineHash: '067b7a6505c286281f54ac004207c221'
};
