'use strict';
var fs = require('fs');
var moment = require('moment');
module.exports = {
	restRolesUrl: 'https://rest.sandbox.netsuite.com/rest/roles',
	filterPoRecords: false,
	pdfSplitFactor: 4,
	counterLog: 25,
	maxRecords: 1,
	//testRequestKeys: ['18086', '1803', '18037'],
	dataLocation: 'd:\\blackline\\data\\',
	concurDataLocation: 'd:\\concurapi\\tempdata\\',
	concurLog: 'd:\\concurapi\\tempconcurcount.txt',
	//dataLocation: 'C:\\Work\\Simplion\\Clients\\RC\\Blackline Integration\\rc-blackline\\data\\',
	concurRestletUrl: '/app/site/hosting/restlet.nl?script=' + 
		'customscript_smpln_rc_integ_data_entry&deploy=customdeploy_smpln_rc_integ_data_entry',
	netSuiteRestlet:{
		username: 'sanjeev.kumar@simplion.com',
		password: 'my@netsuite@08',
		role: '3',
		accountNumber: '859277',
		smplnClassName : 'connector.netsuiterestlet'
	},
	mailer: {
		options: {
			host : 'sjc01-c01-mrs01.c01.ringcentral.com',
			ignoreTLS: true
		},
		from: 'no-reply@ringcentral.com',
		blacklineAlertReceiver: 'prasun.sultania@simplion.com,sanjeev.kumar@simplion.com,francis.lee@ringcentral.com',
		errorReceiver: 'prasun.sultania@simplion.com,sanjeev.kumar@simplion.com,francis.lee@ringcentral.com',
		alertReceiver: 'francis.lee@ringcentral.com,prasun.sultania@simplion.com,sanjeev.kumar@simplion.com'//concur only
	}
};