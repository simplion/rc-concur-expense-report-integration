/* jshint node: true */
'use strict';
var fs = require('fs');
var fileData = fs.readFileSync('./config/sql.txt');
var DateUtils = require('../../utils/utils.date');
var aDatePast = new Date(new Date().getTime() + 1000*3600*24);
var moment = require('moment');
fileData = fileData.toString();
fileData = fileData.replace(/\n/g, ' ');
module.exports = {
	restRolesUrl: 'https://rest.sandbox.netsuite.com/rest/roles',
	invoicePrefix: 'TEST_Lirik_',
	// filterNonPoRecords: false,
	// filterNonPoLineNumber: false,
	filterPoRecords: false,
	pdfSplitFactor: 4,
	// maxRecords: 1,
	// testRequestKeys: [/*'31053',*/ '398861'], //15486
	jarLocation: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\rc-ns-jdbc.jar',
	concurDataLocation: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\ConcurData\\',
	dataLocation: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\data\\',
	mesLog: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\logs\\mescount.txt',
	mesCbLog: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\logs\\mescbcount.txt',
	netsuiteLog: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\logs\\netsuitecount.txt',
	concurLog: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\logs\\concurcount.txt',
	counterLog: 2,
	mesTimeout: 600000, //time in ms 6000000ms = 10min

	expenseDataLocation: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\ExpenseData\\',
	expenseLog: 'D:\\Work\\Simplion\\Clients\\RC\\Concur Integration\\logs\\expensecount.txt',
	expenseRestletUrl: '/app/site/hosting/restlet.nl?script=' +
		'customscript_smpln_rc_integ_data_entry&deploy=customdeploy_smpln_rc_integ_data_entry',

	netSuiteRestlet:{
		/*appPublic: '8f417e05bfbbecc10e293cf466e3049da137fd0007df675c5e61f5d9b0baeb68',
		appSecret: '596b1680389dac312957841e2a2b4971723c28e293526780866582cf10753ffd',
		tokenPublic: '9dd4583ba3113d2085b9006e890cf075d51cd887ec1ed8d7e0eb5f28e5fd1571',
		tokenSecret: '3e336e5d36ac219185e60ed29ba30a809972887ce79094362f3efb50131387e0',
		accountNumber: '859277',*/
		username: 'mohit.kumar@lirik.io',
		password: 'netsuite@123',
		role: '3',
		appPublic: '49f0fdd5915a043cff1a441a86b5c55188e4a34eb2c5922546a4f64459c89e88',
		appSecret: '2bc01ef737ac2d539d893bbb16cd4acebc6a29f5e92e7744c08013dbb5a8a052',
		tokenPublic: '056872f816991aa79a9942d356f83c6844c43ebbdbc585cba75275f5d4118580',
		tokenSecret: 'd5e3f3fd4907ed82a4aafd9f0d6d3c3c9a3919ce85aec12dbbab8d001d392de4',
		accountNumber: '859277',
		smplnClassName : 'connector.netsuiterestlet'
	},
	concurRestletUrl: '/app/site/hosting/restlet.nl?script=' +
		'customscript_smpln_rc_integ_data_entry&deploy=customdeploy_smpln_rc_integ_data_entry',
	netSuiteOdbcConnector: {
		smplnClassName: 'connector.netsuiteodbcconnector',
		nsEmail: 'mohit.kumar@lirik.io',
		nsPassword: 'netsuite@123',
		accountNumber: '859277',
		roleId: '18'
	},
	netsuiteData: {
		smplnClassName: 'connector.data.netsuitedata',
		sql: fileData,
		startDate: (process.argv[3] || '2015-02-03'),
		endDate: (process.argv[4] || '2015-02-04'),
		parallelProcesses: 3,
		fetchSize: 100
	},
	blacklineNetSuiteData: {
		smplnClassName: 'connector.data.blacklinedata',
		folderPath: '/SBNA/NetSuite/GLTrans/'
	},
	mesData:{
		smplnClassName: 'connector.data.mesdata',
		reportDateBegin: (moment(process.argv[3] || '2015-01-01', 'YYYY-MM-DD').format('MM/DD/YYYY')),
		reportDateEnd: (moment(process.argv[4] || '2015-01-04', 'YYYY-MM-DD').format('MM/DD/YYYY')),
		nodeId: 941000110603,
		reportType: 1,
		includeTridentTranId: false,
		includePurchaseId: true,
		includeClientRefNum: false,
		dsReportId: 2
	},
	mesCbData:{
		smplnClassName: 'connector.data.mesdata',
		reportDateBegin: (moment(process.argv[3] || '2015-01-01', 'YYYY-MM-DD').format('MM/DD/YYYY')) ,
		reportDateEnd: (moment(process.argv[4] || '2015-02-01' , 'YYYY-MM-DD').format('MM/DD/YYYY')),
		nodeId: 941000110603,
		reportType: 1,
		includeTridentTranId: false,
		includePurchaseId: true,
		includeClientRefNum: false,
		dsReportId: 5,
		chargeBack: true
	},
	blacklineMesData: {
		smplnClassName: 'connector.data.blacklinedata',
		folderPath: '/SBNA/MES/'
	},
	blacklineMesCbData: {
		smplnClassName: 'connector.data.blacklinedata',
		folderPath: '/SBNA/MESCB/'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'no-reply@lirik.io',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'mohit.kumar@lirik.io',
				pass: process.env.MAILER_PASSWORD
			}
		},
		errorReceiver: 'mohit.kumar@lirik.io',
		alertReceiver: 'mohit.kumar@lirik.io',
		blacklineAlertReceiver: 'mohit.kumar@lirik.io,sanjeev.kumar@lirik.io',
	}
};
