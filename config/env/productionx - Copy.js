'use strict';
var fs = require('fs');
var fileData = fs.readFileSync('D:/blackline/rc-blackline/config/sql.txt');
var DateUtils = require('../../utils/utils.date');
var aDatePast = new Date(new Date().getTime() + 1000*3600*24);
var moment = require('moment');
fileData = fileData.toString();
fileData = fileData.replace(/\n/g, ' ');

module.exports = {
	restRolesUrl: 'https://rest.netsuite.com/rest/roles',
	invoicePrefix: '',
	filterPoRecords: true,
	pdfSplitFactor: 4,
	jarLocation: 'd:\\blackline\\rc-ns-jdbc.jar',
	concurDataLocation: 'd:\\concurapi\\data\\',
	dataLocation: 'd:\\blackline\\data\\',
	mesLog: 'd:\\blackline\\mescount.txt',
	mesCbLog: 'd:\\blackline\\mescbcount.txt',
	netsuiteLog: 'd:\\blackline\\netsuitecount.txt',
	concurLog: 'd:\\concurapi\\concurcount.txt',
	counterLog: 50,
	netSuiteRestlet:{
		username: 'rcpodbc@ringcentral.com',
		password: 'E3iL4MKz89',
		role: '18',
		//appPublic: 'c34500017b0e92b320f158729c325e76cb3075fe7f7c39d3c91a2f40afdb8a9c',
		//appSecret: '0133f06dfa18d70c15f4fd0ed4fc1452ff960b0edcddc62a78535a73258b9193',
		//tokenPublic: '789e26b20892f92cc119198c6529e65e1b6bb7daf4390faea99c2a8126c2e967',
		//tokenSecret: '4a091b7396d7df74e0bfb9b42773c68acfaf08aea0a0a322b3347940dfceb732',
		accountNumber: '859277',
		smplnClassName : 'connector.netsuiterestlet'
	},
	concurRestletUrl: '/app/site/hosting/restlet.nl?script=customscript_smpln_rc_integ_data_entry&deploy=customdeploy_smpln_rc_integ_data_entry',
	netSuiteOdbcConnector: {
		smplnClassName: 'connector.netsuiteodbcconnector',
		nsEmail: 'rcpodbc@ringcentral.com',
		nsPassword: 'E3iL4MKz89',
		accountNumber: '859277',
		roleId: '18'
	},
	netsuiteData: {
		smplnClassName: 'connector.data.netsuitedata',
		sql: fileData,
		startDate: (process.argv[3] || DateUtils.getTodayInYyyyMmDd('-').replace(/(\d\d\d\d-\d\d-)\d\d/, '$101')),
		endDate: (process.argv[4] || DateUtils.getDateInYyyyMmDd(aDatePast,'-')),
		parallelProcesses: 5,
		fetchSize: 200
	},
	blacklineNetSuiteData: {
		smplnClassName: 'connector.data.blacklinedata',
		folderPath: '/PROD/NetSuite/GLTrans/'
	},
	blacklineMesData: {
		smplnClassName: 'connector.data.blacklinedata',
		folderPath: '/PROD/MES/'
	},
	blacklineMesCbData: {
		smplnClassName: 'connector.data.blacklinedata',
		folderPath: '/PROD/MESCB/'
	},
	mesData:{
		smplnClassName: 'connector.data.mesdata',
		reportDateBegin: (moment(process.argv[3], 'YYYY-MM-DD').format('MM/DD/YYYY') || 
				DateUtils.getTodayInYyyyMmDd('-').replace(/(\d\d\d\d-\d\d-)\d\d/, '$101')),
		reportDateEnd: (moment(process.argv[4], 'YYYY-MM-DD').format('MM/DD/YYYY') || DateUtils.getDateInYyyyMmDd(aDatePast,'-')),
		nodeId: 941000110603,
		reportType: 1,
		includeTridentTranId: false,
		includePurchaseId: true,
		includeClientRefNum: false,
		dsReportId: 2
	},
	mesCbData:{
		smplnClassName: 'connector.data.mesdata',
		reportDateBegin: (moment(process.argv[3], 'YYYY-MM-DD').format('MM/DD/YYYY') || DateUtils.getTodayInYyyyMmDd('-').replace(/(\d\d\d\d-\d\d-)\d\d/, '$101')),
		reportDateEnd: (moment(process.argv[4], 'YYYY-MM-DD').format('MM/DD/YYYY') || DateUtils.getDateInYyyyMmDd(aDatePast,'-')),
		nodeId: 941000110603,
		reportType: 1,
		includeTridentTranId: false,
		includePurchaseId: true,
		includeClientRefNum: false,
		dsReportId: 5,
		chargeBack: true
	},
	mailer: {
		options: {
			host : 'sjc01-c01-mrs01.c01.ringcentral.com',
			ignoreTLS: true
		},
		from: 'no-reply@ringcentral.com',
		blacklineAlertReceiver: 'prasun.sultania@simplion.com,sanjeev.kumar@simplion.com,francis.lee@ringcentral.com',
		errorReceiver: 'prasun.sultania@simplion.com,sanjeev.kumar@simplion.com,francis.lee@ringcentral.com',
		alertReceiver: 'francis.lee@ringcentral.com,kim.vu@ringcentral.com,jane.yang@ringcentral.com,peter.tseng@ringcentral.com,prasun.sultania@simplion.com,sanjeev.kumar@simplion.com'//concur only
	}
};