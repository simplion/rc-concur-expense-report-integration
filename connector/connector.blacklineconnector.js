var Serializable = require('../utils/utils.serializable');
var util = require('util');
var Q = require('Q');
var Client = require('ssh2').Client;
var fs = require('fs');
var config = require('../config/config');

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function BlacklineConnector(config){
	BlacklineConnector.super_.call(this, config);
	this.host = config.host;
	this.username = config.username;
	this.password = config.password;
	this.port = config.port;
	this.folder = config.folder;
	
	this.smplnClassName = 'connector.blacklineconnector';
}

util.inherits(BlacklineConnector, Serializable);

BlacklineConnector.prototype.getHost = function(){
	return this.host;
};
BlacklineConnector.prototype.setHost = function(host){
	this.host = host;
};
BlacklineConnector.prototype.getUsername = function(){
	return this.username;
};
BlacklineConnector.prototype.setUsername = function(username){
	this.username = username;
};
BlacklineConnector.prototype.getPassword = function(){
	return this.password;
};
BlacklineConnector.prototype.setPassword = function(password){
	this.password = password;
};
BlacklineConnector.prototype.getPort = function(){
	return this.password;
};
BlacklineConnector.prototype.setPort = function(port){
	this.port = port;
};
BlacklineConnector.prototype.getFolder = function(){
	return this.folder;
};
BlacklineConnector.prototype.setFolder = function(folder){
	this.folder = folder;
};

BlacklineConnector.prototype.upload = function(file, remoteFullPath){
	var deferred = Q.defer(),
		streamEnded = false;
	console.log('Conecting to FTP ... ');
	
	try{
		var sftpClient = new Client();
	    var creds = {
	    	host: this.host,
	    	port: this.port,
	    	username: this.username,
	    	password: this.password,
	    	hostHash: 'md5',
	    	hostVerifier: function(hash){
	    		console.log(hash);
	    		return hash === config.blacklineHash;
	    	}
	    };
	    
	    sftpClient.on('error', function(err){
	    	console.log('FTP Connection failed');
	    	return deferred.reject(err);
	    }).on('timeout', function(err){
	    	console.log('FTP Connection Timed out');
	    	return deferred.reject(err || 'Connection to Blackline FTP Timed out.');
	    }).on('end', function(err){
	    	console.log('FTP Connection Ended');
	    	if(!streamEnded)
	    		return deferred.reject(err || 'Connection to Blackline FTP Ended by server');
	    }).on('ready', function() {
	        console.log('SFTP Client :: ready');
	        sftpClient.sftp(function(err, sftp) {
	        	var writeStream,
	        		hadError = false;
	        	
	        	if (err) {
	        		return deferred.reject(err);
	        	}
	        	
	        	console.log('begin stream');
	        	var t1 = new Date();
	            writeStream = sftp.createWriteStream(remoteFullPath).on('error', function(err){
	            	console.log('ftp upload errored!');
	            	hadError = true;
	            	return deferred.reject(err);
	            }).on('finish', function(){
	            	console.log('total time (mins):: ' + (new Date() - t1)/(60000));
	            	console.log('ftp upload done!');
	            	streamEnded = true;
	            	sftpClient.end();
	            	if(!hadError){
	            		return deferred.resolve(err);
	            	}
	            	return;
	            });
	            
	            try {
	            	fs.createReadStream(file).pipe(writeStream);
	            } catch(ex){
	            	console.log('ftp upload errored!');
	            	hadError = true;
	            	sftpClient.end();
	            	return deferred.reject(ex.message);
	            }
	        });
	      }).connect(creds);
	} catch(ex){
		console.log('An unexpected FTP error has occured');
		return setTimeout(function(){
			deferred.reject(ex.message);
		});
	}
	
	return deferred.promise;
};

module.exports = BlacklineConnector;