var Serializable = require('../utils/utils.serializable');
var util = require('util');

/**
 * 
 * @param config
 * @returns
 */
function AbstractPlatformConnector(config){
	AbstractPlatformConnector.super_.call(this, config);
	this.endPointName = config.endPointName;
	this.smplnClassName = 'connector.abstractplatformconnector';
}

util.inherits(AbstractPlatformConnector, Serializable);

AbstractPlatformConnector.prototype.getEndPointName = function(){
	return this.endPointName;
};

AbstractPlatformConnector.prototype.setEndPointName = function(endPointName){
	this.endPointName = endPointName;
};

AbstractPlatformConnector.prototype.connect = function(){
	throw new Error("Abstract Method connect must be overriden");
};

AbstractPlatformConnector.prototype.close = function(){
	throw new Error("Abstract Method close must be overriden");
};

/**
 * Grab data from a system
 * @param {AbstractData} required dataConfig for grabbing the data
 * @return {Object} data as JSON/CSV/String as returned by the EndPoint
 */
AbstractPlatformConnector.prototype.readData = function(dataConfig){
	throw new Error("Abstract Method readData must be overriden");
};

module.exports = AbstractPlatformConnector;