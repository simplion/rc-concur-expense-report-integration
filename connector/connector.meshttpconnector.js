var AbstractPlatformConnector = require('./connector.AbstractPlatformConnector');
var util = require('util');
var Q = require('Q');
var fs = require('fs');
var spawn = require('child_process').spawn;
var config = require('../config/config');
var concatFiles = require('../utils/utils.files').concatFiles;
var MES_URL = 'https://www.merchante-solutions.com/jsp/reports/report_api.jsp';
/**
 * Represents the platform data, configuration required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function MesHttpConnector(config){
	MesHttpConnector.super_.call(this, config);
	this.endPointName = 'Mes';
	this.userId = config.userId;
	this.userPass = config.userPass;
	this.smplnClassName = 'connector.meshttpconnector';
};

//console.log('loading ' + typeof AbstractPlatformConnector);
util.inherits(MesHttpConnector, AbstractPlatformConnector);

MesHttpConnector.prototype.getUserId = function(){
	return this.userId;
};

MesHttpConnector.prototype.setUserId = function(userId){
	this.userId = userId;
};

MesHttpConnector.prototype.getUserPass = function(){
	return this.userPass;
};

MesHttpConnector.prototype.setUserPass = function(userPass){
	this.userPass = userPass;
};

MesHttpConnector.prototype.readData = function(mesData){
	var deferred = Q.defer();
	var request = require('request');
	var fs = require('fs');
	var errored = false;
	var counter = 0;
	var str =  {
		'userId': this.getUserId(),
		'userPass': this.getUserPass(),
		'reportDateBegin':mesData.getReportDateBegin(),
		'reportDateEnd': mesData.getReportDateEnd(),
		'nodeId': mesData.getNodeId(),
		'reportType': mesData.getReportType(),
		'includeTridentTranId': mesData.getIncludeTridentTranId(),
		'includePurchaseId': mesData.getIncludePurchaseId(),
		'includeClientRefNum': mesData.getIncludeClientRefNum(),
		'dsReportId':mesData.getDsReportId()
	};
	request.post({uri:MES_URL, qs:str, timeout: (config.mesTimeout || 720000)})
		.on('error', function(err) {
			errored = err;
			console.dir(err);
			deferred.reject(err);
  })
   .on('end', function(err) {
	   if(err || errored){
		   return deferred.reject(err);
	   }
	   console.log('Mes(cb) download complete.')
    deferred.resolve(config.dataLocation + 'mes.csv');
  })
  .on('data', function(){
	  counter++;
	  if(counter % (config.counterLog || 100) === 0){
		  console.log('still downloading mes(cb) Http... Chunks so, far ::' + counter);
	  }
  })
  .pipe(fs.createWriteStream(config.dataLocation + 'mes.csv'));
	
	console.log('Started downloading mes(cb) data. Date Begin: ' + mesData.getReportDateBegin() + '. Date End:' + mesData.getReportDateEnd());
	return deferred.promise;
};

module.exports = MesHttpConnector;