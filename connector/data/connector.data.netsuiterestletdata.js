var Serializable = require('../../utils/utils.serializable');
var util = require('util');
var _ = require('lodash');

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function NetSuiteRestletData(config){

	NetSuiteRestletData.super_.call(this, config);

	this.url = config.url;
	this.httpMethod = config.httpMethod;
	this.body = config.body;
	this.record = config.record;
	this.smplnClassName = 'connector.data.netsuiterestletdata';
}

util.inherits(NetSuiteRestletData, Serializable);


NetSuiteRestletData.prototype.setUrl = function(url){
	this.url = url;
};

NetSuiteRestletData.prototype.getUrl = function(){
	return this.url;
};

NetSuiteRestletData.prototype.setRecord = function(record){
	this.record = record;
};

NetSuiteRestletData.prototype.getRecord = function(){
	return this.record;
};

NetSuiteRestletData.prototype.setHttpMethod = function(httpMethod){
	this.httpMethod = httpMethod;
};

NetSuiteRestletData.prototype.getHttpMethod = function(){
	return this.httpMethod;
};

NetSuiteRestletData.prototype.setBody = function(body){
	this.body = body;
};

NetSuiteRestletData.prototype.getBody = function(){
	return this.body;
};

module.exports = NetSuiteRestletData;
