var AbstractPlatformData = require('./connector.data.abstractplatformdata');
var util = require('util');
var _ = require('lodash');
var dateUtils = require('../../utils/utils.date');

var EXTRACT_DEFINITIONS = [
    'Standard Payment Request Accounting Extract v2.1 (ONP)',
    'Standard Payment Request Accounting Extract v2.1 (Emerg Run)',
    'AP/GL Extract V.2.4.4',
    'Standard Employee Requested Vendor Extract',
    'Invoice Payment Confirmation v1.0',
    'AP/GL Extract V.2.4.4 - TEST'
];

/**
 * Represents the platform data, configuration required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function ExpenseData(config) {

    ExpenseData.super_.call(this, config);

    //will create the extract and download the file for the specified extract
    if (config.extract && EXTRACT_DEFINITIONS.indexOf(config.extract) === -1) {
        throw new Error('Invalid Extract name - ' + extract);
    }
    this.extract = config.extract;

    this.jobIds = config.jobIds;
    this.fileNames = config.fileNames;
    // Start, added by Mohit Kumar for zipped file on 10th Aug 2016
    this.filePaths = config.filePaths;
    // END, added by Mohit Kumar for zipped file on 10th Aug 2016

    this.jobId = config.jobId;
    this.definitionId = config.definitionId;
    this.jobDateTimes = config.jobDateTimes;

    this.startTime = config.startTime;
    this.endTime = config.endTime;

    this.smplnClassName = 'connector.data.expensedata';
}

util.inherits(ExpenseData, AbstractPlatformData);

ExpenseData.prototype.setFileNames = function(fileNames) {
    this.fileNames = fileNames;
};

ExpenseData.prototype.getFileNames = function() {
    return this.fileNames;
};

// Start, added by Mohit Kumar for zipped file on 10th Aug 2016
ExpenseData.prototype.setFilePaths = function(filePaths) {
    this.filePaths = filePaths;
};

ExpenseData.prototype.getFilePaths = function() {
    return this.filePaths;
};
// END, added by Mohit Kumar for zipped file on 10th Aug 2016

ExpenseData.prototype.setJobDateTimes = function(jobDateTimes) {
    this.jobDateTimes = jobDateTimes;
};

ExpenseData.prototype.getJobDateTimes = function() {
    return this.jobDateTimes;
};

ExpenseData.prototype.setStartTime = function(startTime) {
    this.startTime = startTime;
};

ExpenseData.prototype.getStartTime = function() {
    return this.startTime;
};

ExpenseData.prototype.setEndTime = function(endTime) {
    this.endTime = endTime;
};

ExpenseData.prototype.getEndTime = function() {
    return this.endTime;
};

ExpenseData.prototype.setExtract = function(extract) {
    if (EXTRACT_DEFINITIONS.indexOf(extract) === -1) {
        throw new Error('Invalid Extract name - ' + extract);
    }
    this.extract = extract;
};

ExpenseData.prototype.getExtract = function() {
    return this.extract;
};

ExpenseData.prototype.setDefinitionId = function(definitionId) {
    this.definitionId = definitionId;
};

ExpenseData.prototype.getDefinitionId = function() {
    return this.definitionId;
};

ExpenseData.prototype.setJobId = function(jobId) {
    this.jobId = jobId;
};

ExpenseData.prototype.getJobId = function() {
    return this.jobId;
};

ExpenseData.prototype.setJobIds = function(jobIds) {
    this.jobIds = jobIds;
};

ExpenseData.prototype.getJobIds = function() {
    return this.jobIds;
};

module.exports = ExpenseData;
