var Serializable = require('../../utils/utils.serializable');
var util = require('util');
var _ = require('lodash');

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function BlacklineData(config){
	
	BlacklineData.super_.call(this, config);
	
	this.folderPath = config.folderPath;
	this.smplnClassName = 'connector.data.blacklinedata';
}

util.inherits(BlacklineData, Serializable);

BlacklineData.prototype.setFolderPath = function(folderPath){
	this.folderPath = folderPath;
};

BlacklineData.prototype.getFolderPath = function(){
	return this.folderPath;
};

module.exports = BlacklineData;
