var AbstractPlatformData = require('./connector.data.abstractplatformdata');
var util = require('util');
var _ = require('lodash');
var dateUtils = require('../../utils/utils.date');

var EXTRACT_DEFINITIONS = [
	'Standard Payment Request Accounting Extract v2.1 (ONP)',
	'Standard Payment Request Accounting Extract v2.1 (Emerg Run)',
	'AP/GL Extract V.2.4.4',
	'Standard Employee Requested Vendor Extract',
	'Invoice Payment Confirmation v1.0',
	'AP/GL Extract V.2.4.4 - TEST'
];

/**
 * Represents the platform data, configuration required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function ConcurData(config){

	ConcurData.super_.call(this, config);

	//will create the extract and download the file for the specified extract
	if(config.extract && EXTRACT_DEFINITIONS.indexOf(config.extract) === -1){
		throw new Error('Invalid Extract name - ' + extract);
	}
    this.extract = config.extract;

    this.jobIds = config.jobIds;
    this.fileNames = config.fileNames;
    this.jobId = config.jobId;
    this.definitionId = config.definitionId;
    this.jobDateTimes = config.jobDateTimes;

    this.startTime = config.startTime;
    this.endTime = config.endTime;

    this.smplnClassName = 'connector.data.concurdata';
}

util.inherits(ConcurData, AbstractPlatformData);

ConcurData.prototype.setFileNames = function(fileNames){
	this.fileNames = fileNames;
};

ConcurData.prototype.getFileNames = function(){
	return this.fileNames;
};

ConcurData.prototype.setJobDateTimes = function(jobDateTimes){
	this.jobDateTimes = jobDateTimes;
};

ConcurData.prototype.getJobDateTimes = function(){
	return this.jobDateTimes;
};

ConcurData.prototype.setStartTime = function(startTime){
	this.startTime = startTime;
};

ConcurData.prototype.getStartTime = function(){
	return this.startTime;
};

ConcurData.prototype.setEndTime = function(endTime){
	this.endTime = endTime;
};

ConcurData.prototype.getEndTime = function(){
	return this.endTime;
};

ConcurData.prototype.setExtract = function(extract){
	if(EXTRACT_DEFINITIONS.indexOf(extract) === -1){
		throw new Error('Invalid Extract name - ' + extract);
	}
	this.extract = extract;
};

ConcurData.prototype.getExtract = function(){
	return this.extract;
};

ConcurData.prototype.setDefinitionId = function(definitionId){
	this.definitionId = definitionId;
};

ConcurData.prototype.getDefinitionId = function(){
	return this.definitionId;
};

ConcurData.prototype.setJobId = function(jobId){
	this.jobId = jobId;
};

ConcurData.prototype.getJobId = function(){
	return this.jobId;
};

ConcurData.prototype.setJobIds = function(jobIds){
	this.jobIds = jobIds;
};

ConcurData.prototype.getJobIds = function(){
	return this.jobIds;
};

module.exports = ConcurData;
