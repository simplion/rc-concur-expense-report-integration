var AbstractPlatformData = require('./connector.data.abstractplatformdata');
var util = require('util');
var _ = require('lodash');
var dateUtils = require('../../utils/utils.date');

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function NetSuiteData(config){

	NetSuiteData.super_.call(this, config);

	this.sql = config.sql;
	this.startDate = config.startDate;
	this.endDate = config.endDate;
	this.fetchSize = config.fetchSize || 100;
	this.parallelProcesses = config.parallelProcesses || 5;
	this.dateBeginDateProperty = 'startDate';
	this.dateEndDateProperty = 'endDate';
	this.smplnClassName = 'connector.data.netsuitedata';
}

util.inherits(NetSuiteData, AbstractPlatformData);

/**
 * Convert data of a system to generic JSON
 * In case of NetSuite data is already JSON, so, simply return the input data
 */
NetSuiteData.prototype.convertToJSON = function(data){
	if(!_.isObject(data)){
		throw new Error("data must be a JSON");
	};
	return data;
};

NetSuiteData.prototype.setSql = function(sql){
	this.sql = sql;
};

NetSuiteData.prototype.getSql = function(){
	return this.sql;
};

NetSuiteData.prototype.setFetchSize = function(fetchSize){
	this.fetchSize = fetchSize;
};

NetSuiteData.prototype.getFetchSize = function(){
	return this.fetchSize;
};

NetSuiteData.prototype.setParallelProcesses = function(parallelProcesses){
	this.parallelProcesses = parallelProcesses;
};

NetSuiteData.prototype.getParallelProcesses = function(parallelProcesses){
	return this.parallelProcesses;
};

NetSuiteData.prototype.getStartDate = function(){
	return this.startDate;
};

NetSuiteData.prototype.setStartDate = function(startDate){
	this.startDate = startDate;
};

NetSuiteData.prototype.getEndDate = function(){
	return this.endDate;
};

NetSuiteData.prototype.setEndDate = function(endDate){
	this.endDate = endDate;
};

NetSuiteData.prototype.readProcessedSql = function(){
	if(!this.sql){
		throw new Error("Query is not yet defined.");
	}

	var dates = dateUtils.getSplittedDates(this.startDate, this.endDate, this.parallelProcesses),
		that = this,
		processedSql = [];

	dates.forEach(function(date){
		var sql = that.sql.replace(/{{startDate}}/g, dateUtils.getSqlDateFromDate(date.d1));
		sql = sql.replace(/{{endDate}}/g, dateUtils.getSqlDateFromDate(date.d2));
		processedSql.push(sql);
	});

	return processedSql;
};

module.exports = NetSuiteData;
