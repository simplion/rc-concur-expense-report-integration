var AbstractPlatformData = require('./connector.data.abstractplatformdata');
var util = require('util');
var _ = require('lodash');
var dateUtils = require('../../utils/utils.date');

/**
 * Represents the platform data, configuration required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function MesData(config){
	
	MesData.super_.call(this, config);
	
    this.reportDateBegin     = config.reportDateBegin;
    this.reportDateEnd       = config.reportDateEnd;
    this.nodeId              = config.nodeId;
    this.reportType          = config.reportType;
    this.includeTridentTranId= config.includeTridentTranId;
    this.includePurchaseId   = config.includePurchaseId;
    this.includeClientRefNum = config.includeClientRefNum;
    this.dsReportId          = config.dsReportId;
	this.chargeBack          = config.chargeBack;
	this.dateBeginDateProperty = 'reportDateBegin';
	this.dateEndDateProperty = 'reportDateEnd';
	this.smplnClassName = 'connector.data.mesData';
} 

util.inherits(MesData, AbstractPlatformData);


MesData.prototype.setReportDateBegin = function(reportDateBegin){
	this.reportDateBegin = reportDateBegin;
};

MesData.prototype.getReportDateBegin = function(){
	return this.reportDateBegin;
};

MesData.prototype.setReportDateEnd = function(reportDateEnd){
	this.reportDateEnd = reportDateEnd;
};

MesData.prototype.getReportDateEnd = function(){
	return this.reportDateEnd;
};

MesData.prototype.setNodeId = function(nodeId){
	this.nodeId = nodeId;
};

MesData.prototype.getNodeId = function(){
	return this.nodeId;
};

MesData.prototype.setReportType = function(reportType){
	this.reportType = reportType;
};

MesData.prototype.getReportType = function(reportType){
	return this.reportType;
};

MesData.prototype.getIncludeTridentTranId = function(){
	return this.includeTridentTranId;
};

MesData.prototype.setIncludeTridentTranId = function(includeTridentTranId){
	this.includeTridentTranId = includeTridentTranId;
};

MesData.prototype.getIncludePurchaseId = function(){
	return this.includePurchaseId;
};

MesData.prototype.setIncludePurchaseId = function(includePurchaseId){
	this.includePurchaseId = includePurchaseId;
};

MesData.prototype.getIncludeClientRefNum = function(){
	return this.includeClientRefNum;
};

MesData.prototype.setIncludeClientRefNum = function(includeClientRefNum){
	this.includeClientRefNum = includeClientRefNum;
};

MesData.prototype.getDsReportId = function(){
	return this.dsReportId;
};

MesData.prototype.setDsReportId = function(dsReportId){
	this.dsReportId = dsReportId;
};

MesData.prototype.getChargeBack = function(){
	return this.chargeBack;
};

MesData.prototype.setChargeBack = function(chargeBack){
	this.chargeBack = chargeBack;
};

module.exports = MesData;
