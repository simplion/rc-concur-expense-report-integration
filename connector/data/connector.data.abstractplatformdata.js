var Serializable = require('../../utils/utils.serializable');
var util = require('util');

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function AbstractPlatformData(config){
	AbstractPlatformData.super_.call(this, config);
	this.smplnClassName = 'connector.data.abstractplatformdata';
}

util.inherits(AbstractPlatformData, Serializable);

AbstractPlatformData.prototype.getDataBeginDate = function(){
	if(!this.dateBeginDateProperty){
		throw new Error('Start date must be defined');
	}
	return this[this.dateBeginDateProperty];
};

AbstractPlatformData.prototype.getDataEndDate = function(){
	if(!this.dateEndDateProperty){
		throw new Error('End date must be defined');
	}
	return this[this.dateEndDateProperty];
};

module.exports = AbstractPlatformData;