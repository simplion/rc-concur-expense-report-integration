var AbstractPlatformConnector = require('./connector.AbstractPlatformConnector');
var util = require('util');
var utils = require('../utils/utils.utils');
var Q = require('Q');
var config = require('../config/config');
var request = require('request');
var OAuth = require('oauth-1.0a');
var _ = require('lodash');
var fs = require('fs');

var SyncLogger = require('../utils/utils.synclogger');

/**
 * Restlet Interface for NetSuite
 * @param config
 * @returns
 */
function NetSuiteRestlet(args) {
    NetSuiteRestlet.super_.call(this, args);

    this.username = args.username;
    this.password = args.password;
    this.role = args.role;
    this.queueCount = args.queueCount || 8;
    this.appPublic = args.appPublic;
    this.appSecret = args.appSecret;
    this.tokenPublic = args.tokenPublic;
    this.tokenSecret = args.tokenSecret;
    this.accountNumber = args.accountNumber;
    this.smplnClassName = 'connector.netsuiterestlet';
}

//console.log('loading ' + typeof AbstractPlatformConnector);
util.inherits(NetSuiteRestlet, AbstractPlatformConnector);

NetSuiteRestlet.prototype.getUsername = function() {
    return this.username;
};

NetSuiteRestlet.prototype.setUsername = function(username) {
    this.username = username;
};

NetSuiteRestlet.prototype.getPassword = function() {
    return this.password;
};

NetSuiteRestlet.prototype.setPassword = function(password) {
    this.password = password;
};

NetSuiteRestlet.prototype.getRole = function() {
    return this.role;
};

NetSuiteRestlet.prototype.setRole = function(role) {
    this.role = role;
};

NetSuiteRestlet.prototype.getQueueCount = function() {
    return this.queueCount;
};

NetSuiteRestlet.prototype.setQueueCount = function(queueCount) {
    this.queueCount = queueCount;
};

NetSuiteRestlet.prototype.getAppPublic = function() {
    return this.appPublic;
};

NetSuiteRestlet.prototype.setAppPublic = function(appPublic) {
    this.appPublic = appPublic;
};

NetSuiteRestlet.prototype.getAccountNumber = function() {
    return this.accountNumber;
};

NetSuiteRestlet.prototype.setAccountNumber = function(accountNumber) {
    this.accountNumber = accountNumber;
};

NetSuiteRestlet.prototype.getAppSecret = function() {
    return this.appSecret;
};

NetSuiteRestlet.prototype.setAppSecret = function(appSecret) {
    this.appSecret = appSecret;
};

NetSuiteRestlet.prototype.getTokenPublic = function() {
    return this.tokenPublic;
};

NetSuiteRestlet.prototype.setTokenPublic = function(tokenPublic) {
    this.tokenPublic = tokenPublic;
};

NetSuiteRestlet.prototype.getTokenSecret = function() {
    return this.tokenSecret;
};

NetSuiteRestlet.prototype.setTokenSecret = function(tokenSecret) {
    this.tokenSecret = tokenSecret;
};

NetSuiteRestlet.prototype.connect = function() {
    var deferred = Q.defer();
    setTimeout(deferred.resolve);
    return deferred.promise;
};

NetSuiteRestlet.prototype.close = function() {
    var deferred = Q.defer();
    setTimeout(deferred.resolve);
    return deferred.promise;
};

/**
 * This method sends multiple concurrent requests to netsuite
 * taking advantage of Restlet allowing multiple concurrent requests per session
 */
NetSuiteRestlet.prototype.readUsingConcurrentRequests = function(netSuiteRestletDataArray) {
    var deferred = Q.defer();

    var successCounts = 0,
        queued = 0,
        total = netSuiteRestletDataArray.length,
        completed = 0,
        errors = [],
        successRecords = [],
        overbilled = [],
        that = this,
        errorCounts = 0;

    var sendRequest = function(restletBody) {
        //console.log('sending a request ____ ');
        if (queued >= that.getQueueCount()) {
            return setTimeout(function() {
                sendRequest(restletBody);
            }, 1000);
        }

        //console.log('queued a request ____ ');
        //console.log(JSON.stringify(restletBody));
        that.readData(restletBody).then(function(body) {
            console.dir(body);
            queued--;
            completed++;

            body = JSON.parse(body);

            if (body.error) {
                errors.push({
                    id: body.id,
                    error: body.error,
                    record: restletBody.record,
                    amount: restletBody.amount
                });
                //console.dir(errors);
            } else {
                console.dir(body);
                //console.log('typeof restleBody.record: ' + restletBody.record);
                successRecords.push(restletBody.record);
                overbilled.push(body.isOverBilled);
                successCounts++;
            }

            if (completed === total) {
                console.log('resolving requests ');
                return deferred.resolve({
                    success: successCounts,
                    errors: errors,
                    successRecords: successRecords,
                    overbilled: overbilled
                });
            } else {
                console.log('completed: ' + completed + ' out of ' + total + ' .Success: ' + successRecords.length);
            }
        }, function(err) {
            console.dir(err);
            errors.push(err);
            queued--;
            completed++;
            errorCounts++;

            if (completed === total) {
                return deferred.resolve({
                    success: successCounts,
                    errors: errors
                });
            }
        });
    };

    if (netSuiteRestletDataArray.length === 0) {
        //console.log('0 records');
    }

    process.nextTick(function() {
        var errored = false;
        if (netSuiteRestletDataArray.length === 0) {
            return deferred.resolve({
                success: successCounts,
                errors: errors,
                successRecords: successRecords
            });
        } else {
            _.each(netSuiteRestletDataArray, function(restletData) {
                if (errored) {
                    return;
                }

                //console.log('restletData.record: ' +  restletData.record);
                //better to read files here than client classes
                if (!_.isString(restletData.body) && _.isObject(restletData.body)) {
                    if (errored) {
                        return;
                    }
                    // Start, commented by Mohit Kumar for Expense Report on 23rd August 2016
                    /*
                    var pdffiles = [];
                    _.each(restletData.body.fileNames, function(fileName) {
                        try {
                            pdffiles.push(fs.readFileSync(config.dataLocation + fileName, 'base64'));
                        } catch (ex) {
                            deferred.reject('Failed to read an Image file: ' + fileName + '. ' + ex.message);
                            errored = true;
                            //One file read error can break entire flow
                            //Ideally file will always exist, but, for now we would just reject it to avoid complicated code control flow
                        }
                    });
                    restletData.body.pdffiles = pdffiles;
                    */
                    // END, commented by Mohit Kumar for Expense Report on 23rd August 2016
                    restletData.body = JSON.stringify(restletData.body);
                }

                if (errored) {
                    return;
                }
                sendRequest(restletData);
            });
        }
    });

    //console.log('records');


    return deferred.promise;
};

NetSuiteRestlet.prototype.readData = function(netSuiteRestletData) {
    //console.log(JSON.stringify(netSuiteRestletData));
    var deferred = Q.defer();
    var that = this;

    utils.getNetSuiteRestletBaseUrl({
        authHeader: utils.getNetSuiteRestletAuthHeader({
            username: that.getUsername(),
            password: that.getPassword(),
            role: that.getRole(),
            accountNumber: that.getAccountNumber()
        })
    }).then(function(baseUrl) {
        //console.log('baseUrl: ' + baseUrl);

        // Start, added DEBUG logs by Mohit Kumar for Expense Report on 18th May 2016
        /*
        SyncLogger.log({
        	DEBUG: JSON.stringify(netSuiteRestletData.body),
        	success: 0,
        	errors: 0,
        	flowType: process.argv[2]
        });
        */
        // process.exit(0);
        // END, added DEBUG logs by Mohit Kumar for Expense Report on 18th May 2016

        request({
            url: baseUrl + netSuiteRestletData.getUrl(),
            method: netSuiteRestletData.getHttpMethod(),
            body: netSuiteRestletData.getBody(),
            headers: utils.getNetSuiteRestletAuthHeader({
                netSuiteRestletData: netSuiteRestletData,
                username: that.getUsername(),
                password: that.getPassword(),
                role: that.getRole(),
                accountNumber: that.getAccountNumber()
            })
        }, function(error, response, body) {
            if (error) {
                console.dir(error);
                return deferred.reject(error);
            } else {
                return deferred.resolve(body);
            }
        });
    }, function(err) {
        console.log('error getting base Url: ' + err);
        return deferred.reject(err);
    });

    return deferred.promise;
};

module.exports = NetSuiteRestlet;
