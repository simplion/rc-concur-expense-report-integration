var AbstractPlatformConnector = require('./connector.AbstractPlatformConnector');
var util = require('util');
var utils = require('../utils/utils.utils');
var Q = require('Q');
var fs = require('fs');
var spawn = require('child_process').spawn;
var config = require('../config/config');
var concatFiles = require('../utils/utils.files').concatFiles;

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function NetSuiteOdbcConnector(config){
	NetSuiteOdbcConnector.super_.call(this, config);
	this.nsEmail = config.nsEmail;
	this.nsPassword = config.nsPassword;
	this.accountNumber = config.accountNumber;
	this.roleId = config.roleId;
	this.endPointName = 'NetSuite';
	this.smplnClassName = 'connector.netsuiteodbcconnector';
};

//console.log('loading ' + typeof AbstractPlatformConnector);
util.inherits(NetSuiteOdbcConnector, AbstractPlatformConnector);

NetSuiteOdbcConnector.prototype.getNsEmail = function(){
	return this.nsEmail;
};

NetSuiteOdbcConnector.prototype.setNsEmail = function(nsEmail){
	this.nsEmail = nsEmail;
};

NetSuiteOdbcConnector.prototype.getNsPassword = function(){
	return this.nsPassword;
};

NetSuiteOdbcConnector.prototype.setNsPassword = function(nsPassword){
	this.nsPassword = nsPassword;
};

NetSuiteOdbcConnector.prototype.getAccountNumber = function(){
	return this.accountNumber;
};

NetSuiteOdbcConnector.prototype.setAccountNumber = function(accountNumber){
	this.accountNumber = accountNumber;
};

NetSuiteOdbcConnector.prototype.getRoleId = function(){
	return this.roleId;
};

NetSuiteOdbcConnector.prototype.setRoleId = function(roleId){
	this.roleId = roleId;
};

NetSuiteOdbcConnector.prototype.connect = function(){
	var deferred = Q.defer();
	setTimeout(deferred.resolve);
	return deferred.promise;
};

NetSuiteOdbcConnector.prototype.close = function(){
	var deferred = Q.defer();	
	setTimeout(deferred.resolve);
	return deferred.promise;
};

NetSuiteOdbcConnector.prototype.readData = function(netSuiteData){
	var deferred = Q.defer();
	console.log('Querying....');
	var fileCreated = 0,
		fileErrored = false,
		that = this,
		error = '',
		fileFlag = false;
	
	var sqls = netSuiteData.readProcessedSql(),
		resolved = 0,
		errored = false;
	
	var t1 = new Date();
	var fileNames = [];
	
	utils.getNetSuiteRestletBaseUrl({
		authHeader: utils.getNetSuiteRestletAuthHeader({
			username : that.getNsEmail(),
			password : that.getNsPassword(),
			role: that.getRoleId(), 
			accountNumber: that.getAccountNumber()
		})
	}).then(function(baseUrl){
		var dataCenter = baseUrl.match(/\.(na[^\.]+)\.netsuite\.com$/);
		
		if(!dataCenter && baseUrl.indexOf('sandbox') !== -1){
			dataCenter = ['', 'sandbox'];
		}
		console.log('baseUrl: ' + baseUrl);
		console.log('dataCenter: ' + dataCenter);
		
		sqls.forEach(function(sql, i){
			//dont fork further processes, if there was an error
			if(errored){
				return;
			}
			
			var java = spawn('java', ['-jar', config.jarLocation, 
			                          that.nsEmail, that.nsPassword, that.accountNumber, that.roleId,
			                          sql,
			                          'jdbc:ns://odbcserver' + (dataCenter ? ('.' + dataCenter[1]) : '') + '.netsuite.com:1708;',
			                          netSuiteData.fetchSize.toString()]);
			
			fileNames.push(config.dataLocation + 'query_' + i + '.csv');
			java.stdout.pipe(fs.createWriteStream(fileNames[i]));

			java.stderr.on('data', function (data) {
			  console.log('stderr: ' + data);
			  error = data;
			});

			java.on('close', function (code) {
				resolved++;
				console.log('Query process ' + i + ' completed with success status :: ' + (code === 0));
				
				//Return error on 1st error
				if(code !== 0){
					errored = true;
					console.log('Total time:' + (new Date() - t1)/1000);
					return deferred.reject(error || 'Unknown error occured in JDBC process.');
				}
				
				//When everything is solved
				if(resolved >= sqls.length){
					console.log('Total time:' + (new Date() - t1)/1000);
					console.log('Merging CSVs...');
					if(sqls.length <= 1){
						return deferred.resolve(fileNames[0]);
					}
					concatFiles(fileNames, config.dataLocation + 'query_netsuite.csv').then(function(){
						return deferred.resolve(config.dataLocation + 'query_netsuite.csv');
					}, function(err){
						errored = true;
						console.log('Total time:' + (new Date() - t1)/1000);
						return deferred.reject(err);
					});  
				}
			});
		});
	}, function(err){
		console.log('failed to find base Url');
		console.dir(err);
		return deferred.reject(err.message || err);
	});
	
	
	
	return deferred.promise;
};

module.exports = NetSuiteOdbcConnector;