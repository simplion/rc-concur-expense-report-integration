var AbstractPlatformConnector = require('./connector.AbstractPlatformConnector');
var util = require('util');
var Q = require('Q');
var fs = require('fs');
var _ = require('lodash');
var config = require('../config/config');
var request = require('request');
var parseString = require('xml2js').parseString;
var DateUtils = require('../utils/utils.date');
var Moment = require('moment');

var SyncLogger = require('../utils/utils.synclogger');

var CONSTS = {
    BASE_URL: 'https://www.concursolutions.com/api',
    PATHS: {
        EXTRACT_LIST: '/expense/extract/v1.0/',
        EXTRACT_FILE: '/expense/extract/v1.0/{DEFINITION_ID}/job/{JOB_ID}/file',
        EXTRACT_JOBS: '/expense/extract/v1.0/{DEFINITION_ID}/job',
        EXTRACT_DETAILS: '/expense/extract/v1.0/{DEFINITION_ID}/job/{JOB_ID}',
        EXTRACT_STATUS: '/expense/extract/v1.0/{DEFINITION_ID}/job/{JOB_ID}/status',
        USER: '/user/v1.0/User/',
        IMAGE_URL: '/image/v1.0/report/'
    },
    DAY_MILLS: 24 * 60 * 60 * 1000
};

/**
 * Represents the platform data, configuration(eg: sql query) required to fetch the data and actual data itself
 * @param config
 * @returns
 */
function ConcurApiConnector(config) {
    ConcurApiConnector.super_.call(this, config);
    this.oauthToken = config.oauthToken;
    this.refreshToken = config.refreshToken;
    this.smplnClassName = 'connector.concurapiconnector';
}

util.inherits(ConcurApiConnector, AbstractPlatformConnector);

ConcurApiConnector.prototype.getOauthToken = function() {
    return this.oauthToken;
};

ConcurApiConnector.prototype.setOauthToken = function(oauthToken) {
    this.oauthToken = oauthToken;
};

ConcurApiConnector.prototype.getRefreshToken = function() {
    return this.refreshToken;
};

ConcurApiConnector.prototype.setRefreshToken = function(refreshToken) {
    this.refreshToken = refreshToken;
};

ConcurApiConnector.prototype.readAuthenticationHeader = function() {
    if (!this.getOauthToken()) {
        throw new Error('OAuth token must be defined!');
    }

    return {
        'Authorization': 'OAuth ' + this.getOauthToken()
    };
};

ConcurApiConnector.prototype.connect = function() {
    var deferred = Q.defer();
    setTimeout(deferred.resolve);
    return deferred.promise;
};

ConcurApiConnector.prototype.close = function() {
    var deferred = Q.defer();
    setTimeout(deferred.resolve);
    return deferred.promise;
};

ConcurApiConnector.prototype.readData = function(concurData) {
    var deferred = Q.defer();


    return deferred.promise;
};

ConcurApiConnector.prototype.readExtractIds = function() {
    var deferred = Q.defer();

    // Start, added by Mohit Kumar for testing
    // console.log('readExtractIds headers :::::' + JSON.stringify(this.readAuthenticationHeader()));
    // console.log('readExtractIds url :::::' + CONSTS.BASE_URL + CONSTS.PATHS.EXTRACT_LIST);
    // END, added by Mohit Kumar for testing

    request({
        headers: this.readAuthenticationHeader(),
        url: CONSTS.BASE_URL + CONSTS.PATHS.EXTRACT_LIST
    }, function(error, response, body) {
        if (error) {
            return deferred.reject({
                error: error,
                body: body
            });
        }
        parseString(body, function(err, result) {
            if (err) {
                return deferred.reject({
                    xmlError: err,
                    body: body
                });
            }

            if (result && result.definitions.definition) {
                return deferred.resolve(result.definitions.definition);
            }

            return deferred.reject({
                error: 'Unexpected XML response',
                body: body,
                xml: result
            });
        });
    });

    return deferred.promise;
};

ConcurApiConnector.prototype.readExtractId = function(concurData) {
    var deferred = Q.defer();

    if (!concurData.getExtract()) {
        return deferred.reject('Extract is not defined on data');
    }

    this.readExtractIds().then(function(definitions) {

        var def = _.find(definitions, function(def) {
            //console.log(def.name[0]);
            return def.name.indexOf(concurData.getExtract()) !== -1;
        });

        if (!def || !def.id) {
            return deferred.reject('Could not find a definition for Extract - ' + concurData.getExtract());
        }

        console.log(def.id[0]);
        concurData.setDefinitionId(def.id[0].match(/.+\/(.+)/)[1]);
        return deferred.resolve(def.id[0].match(/.+\/(.+)/)[1]);

    }, function(err) {
        return deferred.reject(err);
    });

    return deferred.promise;
};

ConcurApiConnector.prototype.readMatchedJobs = function(jobs, concurData) {
    var jobsToReturn = [];

    //console.dir(jobs);

    if (!(Moment.isMoment(concurData.getStartTime()))) {
        throw new Error('From time must be specified');
    }

    jobs.forEach(function(job) {

        if (!job['start-time'] || !job['stop-time']) {
            console.dir(job);
            throw new Error('Job does not have a start time/end time. Start Time: ' + job['start-time'] +
                ' End Time: ' + job['stop-time']);
        }

        if (!concurData.getEndTime() &&
            Moment(job['start-time'][0]).format('YYYYMMDD') === concurData.getStartTime().format('YYYYMMDD')) {
            jobsToReturn.push(job);
            return;
        }

        if ((Moment.isMoment(concurData.getEndTime())) &&
            Moment(job['stop-time'][0]).format('YYYYMMDD') === concurData.getEndTime().format('YYYYMMDD') &&
            Moment(job['start-time'][0]).format('YYYYMMDD') === concurData.getStartTime().format('YYYYMMDD')) {
            jobsToReturn.push(job);
            return;
        }

    });

    if (jobsToReturn.length === 0) {
        throw new Error('No matching jobs found for specified start/end Time');
    }

    return jobsToReturn;
};

ConcurApiConnector.prototype.readLastDaysJob = function(concurData) {

    var deferred = Q.defer();

    //https://www.concursolutions.com/api/expense/extract/v1.0/gWschlhwoCS6rXYaP79eValRu3tR8sD1V4w/job
    var url = CONSTS.BASE_URL +
        CONSTS.PATHS.EXTRACT_JOBS.replace('{DEFINITION_ID}', concurData.getDefinitionId());

    var that = this,
        jobs,
        jobsToReturn = [],
        errored = false,
        fileLinks,
        today = new Date();

    console.log(url);

    // Start, added by Mohit Kumar for testing
    // console.log('readLastDaysJob headers :::::' + JSON.stringify(this.readAuthenticationHeader()));
    // console.log('readLastDaysJob url :::::' + url);
    // END, added by Mohit Kumar for testing

    request({
        headers: this.readAuthenticationHeader(),
        url: url,
        method: 'GET'
    }, function(error, response, body) {
        if (error) {
            return deferred.reject({
                error: error,
                body: body
            });
        }
        parseString(body, function(err, result) {
            if (err) {
                return deferred.reject({
                    xmlError: err,
                    body: body
                });
            }

            if (!result || !result.jobs || !_.isArray(result.jobs.job)) {
                return deferred.reject({
                    error: 'Unexpected XML response',
                    body: body,
                    xml: result
                });
            }

            //console.log(JSON.stringify(result.jobs));

            if (Moment.isMoment(concurData.getStartTime())) {
                jobs = that.readMatchedJobs(result.jobs.job, concurData);
                console.log('matched jobs count  - ' + jobs.length);
                //console.log('matched jobs count  - ' + JSON.stringify(jobs));
            } else {
                //Pick latest job
                console.log('Picking up latest job as date/time not specified');
                jobs = [result.jobs.job[0]];
            }


            //Validate job responses for safe-side
            jobs.forEach(function(job) {
                if (errored) {
                    return;
                }

                if (!_.isString(job.id[0])) {
                    return;
                }

                jobsToReturn.push(job);

                if (!_.isArray(job.status) || !_.isArray(job['start-time'])) {
                    console.log(job.status);
                    console.log(job['start-time']);
                    errored = true;
                    return deferred.reject({
                        error: 'Unexpected XML response. XML jobs must have status.',
                        body: body,
                        xml: result
                    });
                }

                if (job.status[0] !== 'Completed') {
                    errored = true;
                    return deferred.reject({
                        error: 'One or more requested job is not yet complete.',
                        body: body,
                        xml: result
                    });
                }

                if (!_.isArray(job['file-link'])) {
                    errored = true;
                    return deferred.reject({
                        error: 'Unexpected XML response. XML jobs must file link.',
                        body: body,
                        xml: result
                    });
                }
            });


            //jobDate = new Date(job['start-time'][0]);

            /*//within 24 hrs
		  if(today - jobDate > CONSTS.DAY_MILLIS * 1){
			  return deferred.reject({error: 'Latest job is older than 24 hours.', body: body, xml: result});
		  }*/

            //Ids
            concurData.setJobIds(_.map(jobsToReturn, function(j) {
                return j.id[0].match(/.+\/(.+)/)[1];
            }));

            concurData.setFileNames(_.map(jobsToReturn, function(j) {
                return 'concur_' + Moment(j['start-time'][0]).format('YYYYMMDDHHMMSSS') + '.txt';
            }));

            concurData.setJobDateTimes(_.map(jobsToReturn, function(j) {
                return Moment(j['start-time'][0]);
            }));

            //file Ids
            return deferred.resolve(_.map(jobsToReturn, function(j) {
                return j['file-link'][0];
            }));

        });
    });

    return deferred.promise;

};

ConcurApiConnector.prototype.getAllImages = function(records) {
    var deferred = Q.defer();
    var that = this,
        errors = [],
        success = 0,
        allProcessed = false;

    var ids = [];

    //allow processing 0 records smoothly
    setImmediate(function() {
        if (records.length === 0) {
            return deferred.resolve(errors);
        }
        _.each(records, function(record) {
            //console.log(typeof record);
            var id = record.readField('Request ID').getValue();
            that.getImageUrl(id).then(function(url) {
                console.log('Downloading image for: ' + id);
                return that.downloadImage(url, id);
            }, function(err) {
                console.log('Failed to Get image Url for: ' + id);
                errors.push({
                    id: id,
                    error: err
                });
                if ((errors.length + success) === records.length) {
                    allProcessed = true;
                    deferred.resolve(errors);
                }
            }).then(function() {
                if (allProcessed) {
                    return;
                }
                success++;
                console.log('Got image for: ' + id + ' :: SuccessCount ::' + success);
                if ((errors.length + success) === records.length) {
                    allProcessed = true;
                    deferred.resolve(errors);
                }
            }, function(err) {
                if (allProcessed) {
                    return;
                }
                console.log('Failed to Get image for: ' + id);
                errors.push({
                    id: id,
                    error: err
                });
                if ((errors.length + success) === records.length) {
                    allProcessed = true;
                    deferred.resolve(errors);
                }
            });
        });
    });

    return deferred.promise;
};

ConcurApiConnector.prototype.getImageUrl = function(id) {
    var deferred = Q.defer();

    var url = CONSTS.BASE_URL + CONSTS.PATHS.IMAGE_URL + id;
    console.log('image request url: ' + url);

    request({
        'headers': this.readAuthenticationHeader(),
        'url': url,
        'method': 'GET'
    }, function(error, response, body) {
        if (error) {
            return deferred.reject({
                error: error,
                body: body
            });
        }

        parseString(body, function(err, result) {
            if (err) {
                return deferred.reject({
                    xmlError: err,
                    body: body
                });
            }

            if (!result || !result.Image || !result.Image.Url) {
                return deferred.reject({
                    error: 'Unexpected XML response',
                    body: body,
                    xml: result
                });
            }

            return deferred.resolve(result.Image.Url[0]);
        });
    });

    return deferred.promise;
};

ConcurApiConnector.prototype.downloadImage = function(url, id) {
    var deferred = Q.defer();

    var ws = fs.createWriteStream(config.dataLocation + id + '.pdf'),
        errored;

    request.get({
        url: url
    }).on('error', function(err) {
        errored = true;
        ws.end();
        return deferred.reject(err);
    }).on('end', function() {
        ws.end();
        if (errored) {
            return;
        }
        return setTimeout(function() {
            deferred.resolve();
        }, 1000);
    }).pipe(ws);

    return deferred.promise;
};

ConcurApiConnector.prototype.createExtract = function(concurData) {
    var deferred = Q.defer();

    var url = CONSTS.BASE_URL + (CONSTS.PATHS.EXTRACT_JOBS).replace('{DEFINITION_ID}', concurData.getDefinitionId());
    console.log(url);

    request({
        'headers': this.readAuthenticationHeader(),
        'url': url,
        'method': 'POST',
        'Content-Type': 'application/xml',
        'body': '<definition xmlns="http://www.concursolutions.com/api/expense/extract/2010/02">' +
            '<id>' + url + '</id>' +
            '</definition>'
    }, function(error, response, body) {
        if (error) {
            return deferred.reject({
                error: error,
                body: body
            });
        }
        parseString(body, function(err, result) {
            if (err) {
                return deferred.reject({
                    xmlError: err,
                    body: body
                });
            }

            if (!result || !result.job || !result.job.id) {
                return deferred.reject({
                    error: 'Unexpected XML response',
                    body: body,
                    xml: result
                });
            }

            concurData.setJobId(result.job.id[0].match(/.+\/(.+)/)[1]);
            return deferred.resolve(result.job.id[0].match(/.+\/(.+)/)[1]);
        });
    });

    return deferred.promise;
};

ConcurApiConnector.prototype.notifyWhenExtractIsComplete = function(concurData, promise, timer) {

    var deferred = promise || Q.defer();

    var url = CONSTS.BASE_URL +
        CONSTS.PATHS.EXTRACT_STATUS.replace('{DEFINITION_ID}',
            concurData.getDefinitionId()).replace('{JOB_ID}',
            concurData.getJobId());

    timer = timer || 5000;
    that = this;

    console.log(url);

    request({
        headers: this.readAuthenticationHeader(),
        url: url,
        method: 'GET'
    }, function(error, response, body) {
        if (error) {
            return deferred.reject({
                error: error,
                body: body
            });
        }
        parseString(body, function(err, result) {
            if (err) {
                return deferred.reject({
                    xmlError: err,
                    body: body
                });
            }

            if (!result || !result.job || !result.job.status) {
                return deferred.reject({
                    error: 'Unexpected XML response',
                    body: body,
                    xml: result
                });
            }

            console.log('timer :: ' + timer + ' status ::' + result.job.status[0]);
            if (result.job.status[0] !== 'Completed') {
                setTimeout(function() {
                    that.notifyWhenExtractIsComplete(concurData, deferred, timer * 2);
                }, timer);
            } else {
                return deferred.resolve(result.job['file-link'][0]);
            }
        });
    });

    return deferred.promise;

};

ConcurApiConnector.prototype.downloadExtractFile = function(args) {
    var deferred = Q.defer(),
        errored = false;

    var url = CONSTS.BASE_URL +
        CONSTS.PATHS.EXTRACT_FILE.replace('{DEFINITION_ID}',
            args.definitionId).replace('{JOB_ID}',
            args.jobId);

    var ws = fs.createWriteStream(args.responseFile);
    console.log('requesting: ' + url +
        '\nresponseFile' + args.responseFile);

    // Start, added by Mohit Kumar for testing
    // console.log('downloadExtractFile headers :::::' + JSON.stringify(this.readAuthenticationHeader()));
    // console.log('downloadExtractFile url :::::' + url);
    // END, added by Mohit Kumar for testing

    request.get({
        headers: this.readAuthenticationHeader(),
        url: url
    }).on('error', function(err) {
        errored = true;
        ws.end();
        return deferred.reject(err);
    }).on('end', function() {
        ws.end();
        if (errored) {
            return;
        }
        //TODO should return all file paths
        return setTimeout(function() {
            deferred.resolve(args.responseFile);
        }, 1000);
    }).pipe(ws);

    return deferred.promise;
};

ConcurApiConnector.prototype.readExtractFile = function(concurData, i, promise) {
    //TODO change to download all files in concurData
    var deferred = promise || Q.defer(),
        errored = false,
        that = this;

    i = i || 0;

    if (!config.concurDataLocation) {
        setImmediate(function() {
            return deferred.reject('Please make sure Concur data location configuration is defined.');
        });
        return deferred.promise;
    }

    console.log('File for Job : ' + i);
    if (i >= concurData.getJobIds().length) {
        return deferred.resolve(_.map(concurData.getFileNames(), function(fileName) {
            return config.concurDataLocation + fileName;
        }));
    }

    this.downloadExtractFile({
        jobId: concurData.getJobIds()[i],
        definitionId: concurData.getDefinitionId(),
        responseFile: config.concurDataLocation + concurData.getFileNames()[i]
    }).then(function() {
        console.log('File for Job done: ' + i);
        that.readExtractFile(concurData, i + 1, deferred);
    }, function(err) {
        return deferred.reject(err);
    });

    return deferred.promise;
};
module.exports = ConcurApiConnector;
