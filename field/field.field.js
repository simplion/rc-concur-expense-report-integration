var _ = require('lodash');
var util = require('util');
var Serializable = require('../utils/utils.serializable');

/**
 * Class denotes a Field Metadata 
 * @param args
 * @returns {Field}
 */
Field = function(args){
	
	Field.super_.call(this, args);
	
    this._label = args.label;
    this._apiName = args.apiName;
    this._dataType = args.dataType;
    this._readOnly = args.readOnly;
    this._value = args.value;
    this._isMandatory = args.isMandatory;
    
    //Store Information about RecordType being referred, if field is of type reference
    this._refersTo = args.refersTo;
    
    this._timeZone = args.timeZone;
    this._dateFormat = args.dateFormat;
    this._timeFormat = args.timeFormat;
    this._dateTimeFormat = args.dateTimeFormat;
    this.smplnClassName = "field.field";
};

util.inherits(Field, Serializable);

Field.prototype.setRefersTo = function(refersTo){
    this._refersTo = refersTo;
};

Field.prototype.getRefersTo = function(){
    return this._refersTo;
};

Field.prototype.setReadOnly = function(readOnly){
    this._readOnly = readOnly;
};

Field.prototype.isReadOnly = function(){
    return this._readOnly;
};

Field.prototype.setLabel = function(newLabel){
    this._label = newLabel;
};

Field.prototype.setApiName = function(newApiName){
    this._apiName = newApiName;
};

Field.prototype.setDataType = function(newDataType){
    this._dataType = newDataType;
};

Field.prototype.setSelectOptions = function(newSelectOptions){
    this._selectOptions = newSelectOptions;
};

Field.prototype.setValue = function(newValue){
    this._value = newValue;
};

Field.prototype.setIsMandatory = function (newIsMandatory) {
    this._isMandatory = newIsMandatory;
};

Field.prototype.setTimeZone = function(newTimeZone){
    this._timeZone = newTimeZone;
};

Field.prototype.setDateFormat = function (newDateFormat) {
    this._dateFormat = newDateFormat;
};

Field.prototype.setTimeFormat = function (newTimeFormat) {
    this._timeFormat = newTimeFormat;
};

Field.prototype.setDateTimeFormat = function (newDateTimeFormat) {
    this._dateTimeFormat = newDateTimeFormat;
};

Field.prototype.getLabel = function(){
    return this._label;
};

Field.prototype.getApiName = function(){
    return this._apiName;
};

Field.prototype.getDataType = function(){
    return this._dataType;
};

Field.prototype.getSelectOptions = function(){
    return this._selectOptions;
};

Field.prototype.getValue = function(){
    return this._value;
};

Field.prototype.getIsMandatory = function () {
    return this._isMandatory;
};

Field.prototype.getTimeZone = function(){
    return this._timeZone;
};

Field.prototype.getDateFormat = function () {
    return this._dateFormat;
};

Field.prototype.getTimeFormat = function () {
    return this._timeFormat;
};

Field.prototype.getDateTimeFormat = function () {
    return this._dateTimeFormat;
};

module.exports = Field;
