var Serializable = require('../utils/utils.serializable');
var util = require('util');

/**
 * Represents the Mapper
 * @param config
 * @returns
 */
function FieldMapping(config){
	FieldMapping.super_.call(this, config);
	
	this.sourceField = config.sourceField;
	this.targetField = config.targetField;
	this.sourceType = config.sourceType;
	this.targetType = config.targetType;
	
	/** format of the source data in date string might play a vital role**/
	this.sourceFormat = config.sourceFormat;
	this.targetFormat = config.targetFormat;
	
	this.sourceTimeZone = config.sourceTimeZone;
	this.targetTimeZone = config.targetTimeZone;
	
	/** hard coded value**/
	this.hardCodedValue = config.hardCodedValue;
	
	this.transformer = config.transformer;
	
	this.smplnClassName = 'mapper.fieldmapping';
}

util.inherits(FieldMapping, Serializable);

FieldMapping.prototype.setTransformer = function(transformer){
	this.transformer = transformer;
};

FieldMapping.prototype.getTransformer = function(){
	return this.transformer;
};

FieldMapping.prototype.getSourceField = function(){
	return this.sourceField;
};

FieldMapping.prototype.setSourceField = function(sourceField){
	this.sourceField = sourceField;
};

FieldMapping.prototype.getTargetField = function(){
	return this.targetField;
};

FieldMapping.prototype.setTargetField = function(targetField){
	this.targetField = targetField;
};

FieldMapping.prototype.getSourceType = function(){
	return this.sourceType;
};

FieldMapping.prototype.setSourceType = function(sourceType){
	this.sourceType = sourceType;
};

FieldMapping.prototype.getTargetType = function(){
	return this.targetType;
};

FieldMapping.prototype.setTargetType = function(targetType){
	this.targetType = targetType;
};

FieldMapping.prototype.getSourceFormat = function(){
	return this.sourceFormat;
};

FieldMapping.prototype.setSourceFormat = function(sourceField){
	this.sourceFormat = sourceFormat;
};

FieldMapping.prototype.getTargetFormat = function(){
	return this.targetFormat;
};

FieldMapping.prototype.setTargetFormat = function(sourceField){
	this.targetFormat = targetFormat;
};

FieldMapping.prototype.getSourceTimeZone = function(){
	return this.sourceTimeZone;
};

FieldMapping.prototype.setSourceTimeZone = function(sourceTimeZone){
	this.sourceTimeZone = sourceTimeZone;
};

FieldMapping.prototype.getTargetTimeZone = function(){
	return this.targetTimeZone;
};

FieldMapping.prototype.setTargetTimeZone = function(sourceField){
	this.targetTimeZone = targetTimeZone;
};

FieldMapping.prototype.getHardCodedValue = function(){
	return this.hardCodedValue;
};

FieldMapping.prototype.setHardCodedValue = function(hardCodedValue){
	this.hardCodedValue = hardCodedValue;
};
module.exports = FieldMapping;