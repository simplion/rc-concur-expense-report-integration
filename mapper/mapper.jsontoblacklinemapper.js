var config = require('../config/config');
var stringify = require('csv').stringify;
var Q = require('Q');
var _ = require('lodash');

exports.JsonToBlacklineMapper = {
	
	/**
	 * get csv string of one record at a time 
	 */
	getCsvRecord : function(sourceJson, mapping){
		var deferred = Q.defer();
		var fieldMappings = mapping.getFieldMappings();
		var targetCsv = '';
		var fieldVal;
		var data = [[]];
		
		fieldMappings.forEach(function(fieldMapping){
			fieldVal = sourceJson[fieldMapping.getSourceField()];
			
			if(_.isFunction(fieldMapping.getTransformer())){
				fieldVal = fieldMapping.getTransformer()(fieldVal, sourceJson, fieldMappings);
			}
			
			data[0].push(fieldVal);
		});
		
		stringify(data, function(err, output){
			if(err){
				return deferred.reject(err);
			}
			return deferred.resolve(output);
		});
		
		return deferred.promise;
	},
	
	/**
	 * get csv header based on defined mapping
	 */
	getCsvHeaders : function(mapping){
		var deferred = Q.defer();
		var fieldMappings = mapping.getFieldMappings();
		var headers = [[]];
		
		fieldMappings.forEach(function(fieldMapping){
			headers[0].push(fieldMapping.getTargetField());
		});
		
		stringify(headers, function(err, output){
			if(err){
				return deferred.reject(err);
			}
			return deferred.resolve(output);
		});
		
		return deferred.promise;
	}
}