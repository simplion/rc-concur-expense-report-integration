var AbstractMapper = require('./mapper.abstractmapper');
var util = require('util');
var DateUtils = require('../utils/utils.date');
var config = require('../config/config');
var CsvMapper = require('./mapper.csvtojsonmapper');

/**
 * Takes a mapping instance and resolves the mapping 
 * @param config
 * @returns
 */
function NetSuiteOdbcMapper(config){
	NetSuiteOdbcMapper.super_.call(this, config);
	
	this.smplnClassName = 'mapper.netsuiteodbcmapper';
}

util.inherits(NetSuiteOdbcMapper, AbstractMapper);

NetSuiteOdbcMapper.prototype.mapToBlackline = function(sourceFile, netSuiteData){
	var fileName = config.dataLocation + 'nscc_us_extract_' + DateUtils.getTodayInMmDdYyyy() + '.csv';
	return CsvMapper.mapCsvToJson(sourceFile, this.getMapping(), fileName);
};

module.exports = NetSuiteOdbcMapper;