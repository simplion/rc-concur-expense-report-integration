var Q = require('Q');
var csv = require('csv-parser');
var fs = require('fs');
var JsonToBlacklineMapper = require('./mapper.jsontoblacklinemapper').JsonToBlacklineMapper;
exports.mapCsvToJson = function(sourceFile, mapping, fileName){

	var max = 0,
		min = 999999999999,
		errored = false;
	
	var deferred = Q.defer();
	
	JsonToBlacklineMapper.getCsvHeaders(mapping).then(function(headers){
		
		console.log('writing: ' + fileName);
		//console.log('headers: ' + headers);
		//Write headers to file
		fs.writeFile(fileName,
				headers,
			function(err){
				var rows = 0;
			
				if(err){
					console.dir(err);
					errored = true;
					return deferred.reject(err);
				}
				
				console.log('begin pipe: ');
				
				fs.createReadStream(sourceFile).
				pipe(csv()).
				on('data', function(data) {
					
					var heap = process.memoryUsage().heapUsed;
					rows++;
					if(heap > max){
						max = heap;
					}
					if(heap < min){
						min = heap;
					}
					
					if(errored){
						return;
					}
					
					JsonToBlacklineMapper.getCsvRecord(data, mapping).then(function(row){
						fs.appendFile(fileName, row, function(err) {
							if(err){errored = true; return deferred.reject(err);}
							return;
						});
					}, function(err){
						errored = true;
						return deferred.reject(err);
					});
	
				}).on('end', function(){
					
					if(errored){
						return;
					}
					
					console.log('HEAP USAGE WHILE FILE WRITING...' + rows);
					console.log('max Heap (MBs): ' + max/(1024*1024));
					console.log('min Heap (MBs): ' + min/(1024*1024));
					return deferred.resolve({fileName: fileName,rows: rows});
				});
		});
	}, function(err){
		return deferred.reject(err);
	});
	
	return deferred.promise;
	

};