var AbstractMapper = require('./mapper.abstractmapper');
var util = require('util');
var csv = require('csv-parser');
var fs = require('fs');
var JsonToBlacklineMapper = require('./mapper.jsontoblacklinemapper').JsonToBlacklineMapper;
var DateUtils = require('../utils/utils.date');
var config = require('../config/config');
var Q = require('Q');
var CsvMapper = require('./mapper.csvtojsonmapper');
var moment = require('moment');
/**
 * Takes a mapping instance and resolves the mapping 
 * @param config
 * @returns
 */
function MesToFtpMapper(config){
	MesToFtpMapper.super_.call(this, config);
	
	this.smplnClassName = 'mapper.mestoftpmapper';
}

util.inherits(MesToFtpMapper, AbstractMapper);

MesToFtpMapper.prototype.mapToBlackline = function(sourceFile, mesData){
	var fileName = '';
		
	if(!mesData.getChargeBack()){
		fileName = config.dataLocation + 'mes_extract_';
	} else{
		fileName = config.dataLocation + 'mescb_extract_';
	}
	
	//Use today as per Francis
	fileName += moment(/*mesData.getReportDateEnd(), 'MM/DD/YYYY'*/).format('MMDDYYYY') + '.csv';
	return CsvMapper.mapCsvToJson(sourceFile, this.getMapping(), fileName);
	
};

module.exports = MesToFtpMapper;


