var Serializable = require('../utils/utils.serializable');
var util = require('util');

/**
 * Takes a mapping instance and resolves the mapping 
 * @param config
 * @returns
 */
function AbstractMapper(config){
	AbstractMapper.super_.call(this, config);
	
	/** mapper.mapping**/
	this.mapping = config.mapping;
	
	this.smplnClassName = 'mapper.mapping';
}

util.inherits(AbstractMapper, Serializable);

AbstractMapper.prototype.getMapping = function(){
	return this.mapping;
};

AbstractMapper.prototype.setMapping = function(mapping){
	this.mapping = mapping;
};

AbstractMapper.prototype.mapToBlackline = function(mapping){
	throw new Error('Abstract method mapToBlackline must be implemented');
};

module.exports = AbstractMapper;