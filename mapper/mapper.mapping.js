var Serializable = require('../utils/utils.serializable');
var util = require('util');

/**
 * Represents the Mapper
 * @param config
 * @returns
 */
function Mapping(config){
	Mapping.super_.call(this, config);
	
	/** Array of mapper.fieldmapping**/
	this.fieldMappings = config.fieldMappings;
	this.smplnClassName = 'mapper.mapping';
}

util.inherits(Mapping, Serializable);

Mapping.prototype.getFieldMappings = function(){
	return this.fieldMappings;
};

Mapping.prototype.setFieldMappings = function(fieldMappings){
	this.fieldMappings = fieldMappings;
};

module.exports = Mapping;